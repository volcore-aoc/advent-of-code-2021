package set

type Void struct{}
type Set[T comparable] map[T]Void

func (s Set[T]) Clone() Set[T] {
	o := Set[T]{}
	for k, v := range s {
		o[k] = v
	}
	return o
}

func (s Set[T]) Contains(k T) bool {
	_, ok := s[k]
	return ok
}

func (s Set[T]) Add(k T) {
	s[k] = Void{}
}
