package set

type StringSet map[string]Void

func (s StringSet) Clone() StringSet {
	o := StringSet{}
	for k, v := range s {
		o[k] = v
	}
	return o
}

func (s StringSet) IsSet(k string) bool {
	_, ok := s[k]
	return ok
}

func (s StringSet) Set(k string) {
	s[k] = Void{}
}
