package set

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSet(t *testing.T) {
	a := Set[int]{}
	assert.False(t, a.Contains(0))
	assert.False(t, a.Contains(1))
	a.Add(1)
	assert.False(t, a.Contains(0))
	assert.True(t, a.Contains(1))
	b := Set[int64]{}
	assert.False(t, b.Contains(0))
	assert.False(t, b.Contains(1))
	b.Add(1)
	assert.False(t, b.Contains(0))
	assert.True(t, b.Contains(1))

}
