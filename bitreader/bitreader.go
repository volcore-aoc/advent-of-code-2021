package bitreader

import (
	"encoding/hex"
	log "github.com/sirupsen/logrus"
	"strings"
)

func stringToHex(in string) []byte {
	res, err := hex.DecodeString(strings.TrimSpace(in))
	if err != nil {
		log.WithError(err).Errorf("failed to decode hex")
	}
	return res
}

type BitReader struct {
	Data   []byte
	Offset int
}

func NewBitReader(in string) *BitReader {
	return &BitReader{
		Data:   stringToHex(in),
		Offset: 0,
	}
}

func (b *BitReader) ReadInt(num int) int {
	result := 0
	for i := 0; i < num; i++ {
		byteIdx := b.Offset / 8
		byte := b.Data[byteIdx]
		bitIdx := b.Offset % 8
		value := (byte >> (7 - bitIdx)) & 1
		b.Offset++
		result |= int(value) << (num - i - 1)
	}
	return result
}

func (b *BitReader) ReadVarInt() int {
	result := 0
	for {
		v := b.ReadInt(5)
		result = result<<4 | (v & 15)
		if v < 16 {
			break
		}
	}
	return result
}
