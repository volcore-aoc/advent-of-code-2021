package shared

import (
	log "github.com/sirupsen/logrus"
)

func RunDay(f func(string)interface{}, day int64, variant string) {
	data := LoadFile(day)
	result := f(data)
	log.WithField("result", result).Infof("Day %02d%s result", day, variant)
}

