package shared

import (
	"constraints"
)

func ArrayShallowCopy[T any](a []T) []T {
	b := make([]T, len(a))
	for i := range a {
		b[i] = a[i]
	}
	return b
}

func ArrayRange[T constraints.Integer](min T, max T) []T {
	var array []T
	for i := min; i <= max; i++ {
		array = append(array, i)
	}
	return array
}

func ArrayMinMax[T Number](a []T) (T, T) {
	if len(a) == 0 {
		// Haven't found a better way to return the "default" value. T(0) doesn't work. This feels hacky, but works
		return 0, 0
	}
	min := a[0]
	max := a[0]
	for _, v := range a {
		if v < min {
			min = v
		}
		if v > max {
			max = v
		}
	}
	return min, max
}

func ArrayMap[T any](a []T, f func(T) T) []T {
	var out []T
	for _, v := range a {
		out = append(out, f(v))
	}
	return out
}

func ArrayMapCopy[T any, O any](a []T, f func(T) O) []O {
	var out []O
	for _, v := range a {
		out = append(out, f(v))
	}
	return out
}

func ArrayReduce[T any](a []T, start T, f func(T, T) T) T {
	total := start
	for _, v := range a {
		total = f(v, total)
	}
	return total
}

func ArraySum[T Number](a []T) T {
	return ArrayReduce(a, 0, func(t T, v T) T { return t + v })
}

func StringArrayFindOfLength(a []string, l int) string {
	for _, v := range a {
		if len(v) == l {
			return v
		}
	}
	return ""
}
