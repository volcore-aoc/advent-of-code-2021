package shared

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestStringPermutations(t *testing.T) {
	assert.Len(t, StringPermutations("a"), 1)
	assert.Len(t, StringPermutations("ab"), 2)
	assert.Len(t, StringPermutations("abc"), 6)
	for _, v := range StringPermutations("abcd") {
		assert.Len(t, v, 4)
		assert.True(t, strings.ContainsRune(v, 'a'))
		assert.True(t, strings.ContainsRune(v, 'b'))
		assert.True(t, strings.ContainsRune(v, 'c'))
		assert.True(t, strings.ContainsRune(v, 'd'))
	}
}

func TestStringTrimIndex(t *testing.T) {
	assert.Len(t, StringTrimIndex("abc", 0), 2)
	assert.Len(t, StringTrimIndex("abc", 1), 2)
	assert.Len(t, StringTrimIndex("abc", 2), 2)
}

func TestStringContainsAll(t *testing.T) {
	assert.True(t, StringContainsAll("abcdef", "fbca"))
	assert.True(t, !StringContainsAll("abcdef", "fbcaz"))
	assert.True(t, StringContainsAll("abcdef", ""))
	assert.True(t, StringContainsAll("", ""))
}

func TestStringContainsAny(t *testing.T) {
	assert.True(t, StringContainsAny("abcdef", "fbca"))
	assert.True(t, StringContainsAny("abcdef", "fbcaz"))
	assert.True(t, !StringContainsAny("abcdef", ""))
	assert.True(t, !StringContainsAny("", ""))
}
