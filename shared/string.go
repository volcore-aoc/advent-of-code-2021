package shared

import (
	"sort"
	"strings"
)

func FilterString(s string, valid string) string {
	var sb strings.Builder
	for i := range s {
		if strings.Contains(valid, s[i:i+1]) {
			sb.WriteByte(s[i])
		}
	}
	return sb.String()
}

// Removes all letters from a string that are invalid
func InvFilterString(s string, invalid string) string {
	res := ""
	for i := range s {
		if !strings.Contains(invalid, s[i:i+1]) {
			res += s[i : i+1]
		}
	}
	return res
}

func SortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return strings.Join(s, "")
}

func StringPermutations(s string) []string {
	if len(s) <= 1 {
		return []string{s}
	}
	var list []string
	for idx, c := range s {
		for _, sub := range StringPermutations(StringTrimIndex(s, idx)) {
			list = append(list, string(c)+sub)
		}
	}
	return list
}

func StringTrimIndex(s string, index int) string {
	if index == 0 {
		return s[1:]
	}
	if index == len(s)-1 {
		return s[0 : len(s)-1]
	}
	return s[:index] + s[index+1:]
}

func StringContainsAny(s string, letters string) bool {
	return strings.ContainsAny(s, letters)
}

func StringContainsAll(s string, letters string) bool {
	for _, l := range letters {
		if !strings.ContainsRune(s, l) {
			return false
		}
	}
	return true
}

func StringIsUpper(s string) bool {
	return strings.ToUpper(s) == s
}

func StringIsLower(s string) bool {
	return strings.ToLower(s) == s
}
