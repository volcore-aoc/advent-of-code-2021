package shared

import "constraints"

type Number interface {
	constraints.Integer | constraints.Float
}

func Abs[T Number](x T) T {
	if x < 0 {
		return -x
	}
	return x
}
func Min[T Number](a T, b T) T {
	if a < b {
		return a
	} else {
		return b
	}
}

func Max[T Number](a T, b T) T {
	if a > b {
		return a
	} else {
		return b
	}
}

func PositiveMod[T constraints.Integer](x T, mod T) T {
	return (x%mod + mod) % mod
}
