package shared

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"strconv"
	"strings"
)

func LoadFile(day int64) string {
	path := fmt.Sprintf("../../data/day%02d.txt", day)
	d, err := ioutil.ReadFile(path)
	if err != nil {
		log.WithField("day", day).WithError(err).WithField("path", path).Fatalf("Failed to open file")
	}
	return string(d)
}

func LinesToStringArray(in string) []string {
	in = strings.TrimSpace(in)
	return strings.Split(in, "\n")
}

func TakeLines(in string, count int) ([]string, string) {
	in = strings.TrimSpace(in)
	s := strings.SplitN(in, "\n", count+1)
	if len(s) == count+1 {
		return s[:count], s[count]
	} else {
		return s, ""
	}
}

func LinesToInt64Array(in string) []int64 {
	lines := LinesToStringArray(in)
	var out []int64
	for _, line := range lines {
		v := ParseI64(line)
		out = append(out, v)
	}
	return out
}

func LinesToIntArray(in string) []int {
	lines := LinesToStringArray(in)
	var out []int
	for _, line := range lines {
		v := ParseInt(line)
		out = append(out, v)
	}
	return out
}

func CommaListToInt64Array(in string) []int64 {
	in = strings.TrimSpace(in)
	numbers := strings.Split(in, ",")
	var out []int64
	for _, number := range numbers {
		v := ParseI64(number)
		out = append(out, v)
	}
	return out
}

func CommaListToIntArray(in string) []int {
	in = strings.TrimSpace(in)
	numbers := strings.Split(in, ",")
	var out []int
	for _, number := range numbers {
		v := ParseInt(number)
		out = append(out, v)
	}
	return out
}

func GridToIntArrays(in string) [][]int {
	var out [][]int
	for _, line := range LinesToStringArray(in) {
		line = strings.TrimSpace(line)
		var row []int
		for _, c := range line {
			row = append(row, int(c-'0'))
		}
		out = append(out, row)
	}
	return out
}

func ImageToIntArrays(in string) [][]int {
	var out [][]int
	for _, line := range LinesToStringArray(in) {
		line = strings.TrimSpace(line)
		var row []int
		for _, c := range line {
			v := 0
			if c == '#' {
				v = 1
			}
			row = append(row, v)
		}
		out = append(out, row)
	}
	return out
}

func WhitespaceListToIntArray(in string) []int64 {
	in = strings.TrimSpace(in)
	numbers := strings.Fields(in)
	var out []int64
	for _, number := range numbers {
		v := ParseI64(number)
		out = append(out, v)
	}
	return out
}

func WhitespaceListToStringArray(in string) []string {
	in = strings.TrimSpace(in)
	fields := strings.Fields(in)
	var out []string
	for _, v := range fields {
		out = append(out, strings.TrimSpace(v))
	}
	return out
}

func ParseI64(s string) int64 {
	v, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.WithError(err).WithField("string", s).Fatal("Failed to parse int64")
	}
	return v
}

func ParseInt(s string) int {
	return int(ParseI64(s))
}

func ParseBin(s string) int64 {
	v, err := strconv.ParseInt(s, 2, 64)
	if err != nil {
		log.WithError(err).WithField("string", s).Fatal("Failed to parse int64 from binary")
	}
	return v
}
