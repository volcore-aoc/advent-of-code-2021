package day14

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"sort"
	"strings"
	"testing"
)

type context struct {
	template string
	rules    map[string]string
	pairs    map[string]int64
}

func parse(in string) *context {
	lines := shared.LinesToStringArray(in)
	ctx := &context{
		template: lines[0],
		rules:    map[string]string{},
	}
	for _, rule := range lines[2:] {
		parts := strings.Split(rule, " -> ")
		ctx.rules[parts[0]] = parts[1]
	}
	ctx.pairs = map[string]int64{}
	for i := 0; i < len(ctx.template)-1; i++ {
		ctx.pairs[ctx.template[i:i+2]] += 1
	}
	return ctx
}

func (c *context) step() {
	newPairs := map[string]int64{}
	for pair, count := range c.pairs {
		middle := c.rules[pair]
		newPairs[pair[0:1]+middle] += count
		newPairs[middle+pair[1:2]] += count
	}
	c.pairs = newPairs
}

func (c *context) run(count int) {
	for i := 0; i < count; i++ {
		c.step()
	}
}

func (c *context) score() int64 {
	// Only count the first letter of each pair (to avoid double-counting)
	counts := map[uint8]int64{}
	for pair, count := range c.pairs {
		counts[pair[0]] += count
	}
	// Need to add the last letter of the template, as it's always only the second letter of a pair
	counts[c.template[len(c.template)-1]] += 1
	// Figure out the largest and smallest value
	var values []int64
	for _, v := range counts {
		values = append(values, v)
	}
	sort.Sort(shared.Int64Slice(values))
	return values[len(values)-1] - values[0]
}

func partA(in string) interface{} {
	ctx := parse(in)
	ctx.run(10)
	return ctx.score()
}

func partB(in string) interface{} {
	ctx := parse(in)
	ctx.run(40)
	return ctx.score()
}

func Test(t *testing.T) {
	day := int64(14)
	testInput := "NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C"
	assert.EqualValues(t, 1588, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 2188189693529, partB(testInput))
	shared.RunDay(partB, day, "b")
}
