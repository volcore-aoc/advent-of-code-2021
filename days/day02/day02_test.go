package day02

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"gitlab.com/volcore-aoc/advent-of-code-2021/vec"
	"strings"
	"testing"
)

type Int = int64

type day2Instruction struct {
	XYZ *vec.Vec3[Int]
}

func parseDay2(in string) []*day2Instruction {
	in = strings.TrimSpace(in)
	lines := strings.Split(in, "\n")
	var out []*day2Instruction
	for _, line := range lines {
		parts := strings.Split(line, " ")
		distance := shared.ParseI64(parts[1])
		inst := &day2Instruction{}
		switch parts[0] {
		case "forward":
			inst.XYZ = vec.V3[Int](0, 0, distance)
		case "back":
			inst.XYZ = vec.V3[Int](0, 0, -distance)
		case "up":
			inst.XYZ = vec.V3[Int](0, distance, 0)
		case "down":
			inst.XYZ = vec.V3[Int](0, -distance, 0)
		}
		out = append(out, inst)
	}
	return out
}

func day02a(in string) interface{} {
	insts := parseDay2(in)
	xyz := &vec.Vec3[Int]{}
	for _, inst := range insts {
		xyz = xyz.Add(inst.XYZ)
	}
	return shared.Abs(xyz.Y) * shared.Abs(xyz.Z)
}

func day02b(in string) interface{} {
	insts := parseDay2(in)
	aim := &vec.Vec3[Int]{0, 0, 1}
	xyz := &vec.Vec3[Int]{}
	for _, inst := range insts {
		aim = aim.MulAdd(inst.XYZ.Y, vec.V3[Int](0, 1, 0))
		xyz = xyz.MulAdd(inst.XYZ.Z, aim)
	}
	return shared.Abs(xyz.Y) * shared.Abs(xyz.Z)
}

func TestDay02(t *testing.T) {
	assert.EqualValues(t, 150, day02a("forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2"))
	shared.RunDay(day02a, 2, "a")
	assert.EqualValues(t, 900, day02b("forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2"))
	shared.RunDay(day02b, 2, "b")
}
