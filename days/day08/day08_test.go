package day08

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"strings"
	"testing"
)

type pattern struct {
	input  []string
	output []string
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Insane brute force method here

var patterns = []string{
	"abcefg",
	"cf",
	"acdeg",
	"acdfg",
	"bcdf",
	"abdfg",
	"abdefg",
	"acf",
	"abcdefg",
	"abcdfg",
}

func index(c uint8) int {
	return int(c - 'a')
}

func numberForString(s string) int {
	s = shared.SortString(s)
	for k, v := range patterns {
		if s == v {
			return k
		}
	}
	return -1
}

func decode(s string, decoder string) string {
	out := ""
	for _, c := range s {
		out += string(decoder[index(uint8(c))])
	}
	return out
}

func isValidDecoder(input []string, decoder string) bool {
	found := make([]bool, 10)
	for _, v := range input {
		r := decode(v, decoder)
		r = shared.SortString(r)
		for idx, ref := range patterns {
			if r == ref {
				found[idx] = true
			}
		}
	}
	// Check if we found all
	for _, v := range found {
		if !v {
			return false
		}
	}
	return true
}

func (p *pattern) Decode() string {
	// Create all possible permutations of the decoding table, bruteforce
	for _, decoder := range shared.StringPermutations("abcdefg") {
		// Check if this permutation resolves all letters
		if isValidDecoder(p.input, decoder) {
			return decoder
		}
	}
	return ""
}

func (p *pattern) Solve() int {
	decoder := p.Decode()
	// Decode output
	value := 0
	for _, v := range p.output {
		value = value * 10
		value += numberForString(decode(v, decoder))
	}
	return value
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Much faster "decision tree" variant here
// Inspired by https://github.com/matilde-t/AOC-2021/blob/main/Day8/aoc_8_2.py

func (p *pattern) SolveFast() int {
	one := shared.StringArrayFindOfLength(p.input, 2)
	four := shared.StringArrayFindOfLength(p.input, 4)
	seven := shared.StringArrayFindOfLength(p.input, 3)
	eight := shared.StringArrayFindOfLength(p.input, 7)
	// remove seven and four from eight to yield only the bottom left corner
	blCorner := shared.InvFilterString(shared.InvFilterString(eight, four), seven)
	value := 0
	for _, v := range p.output {
		value = value * 10
		switch len(v) {
		case 2:
			value += 1
		case 3:
			value += 7
		case 4:
			value += 4
		case 7:
			value += 8
		case 5:
			// 2, 3, 5
			// 2 is the only one that contains the bottom left corner
			if shared.StringContainsAll(v, blCorner) {
				value += 2
			} else {
				// 3, 5
				// 3 contains 1
				if shared.StringContainsAll(v, one) {
					value += 3
				} else {
					value += 5
				}
			}
		case 6:
			// 6, 9, 0
			// 6 doesn't fully contain 1
			if !shared.StringContainsAll(v, one) {
				value += 6
			} else {
				// 9, 0
				// 0 has the bottom left corner, 9 doesn't
				if shared.StringContainsAll(v, blCorner) {
					value += 0
				} else {
					value += 9
				}
			}
		}
	}
	return value
}

func parse(in string) []*pattern {
	var patterns []*pattern
	for _, line := range shared.LinesToStringArray(in) {
		leftRight := strings.Split(line, " | ")
		patterns = append(patterns, &pattern{
			input:  shared.WhitespaceListToStringArray(leftRight[0]),
			output: shared.WhitespaceListToStringArray(leftRight[1]),
		})
	}
	return patterns
}

func partA(in string) interface{} {
	count := 0
	for _, pattern := range parse(in) {
		for _, out := range pattern.output {
			switch len(out) {
			case 2, 3, 4, 7:
				count++
			}
		}
	}
	return count
}

func partB(in string) interface{} {
	count := 0
	for _, pattern := range parse(in) {
		count += pattern.SolveFast()
	}
	return count
}

func Test(t *testing.T) {
	day := int64(8)
	testInput := "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe\nedbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc\nfgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg\nfbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb\naecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea\nfgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb\ndbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe\nbdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef\negadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb\ngcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce\n"
	testInput2 := "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
	assert.True(t, isValidDecoder(parse(testInput2)[0].input, "cfgabde"))
	assert.False(t, isValidDecoder(parse(testInput2)[0].input, "abcdefg"))
	assert.EqualValues(t, "cfgabde", parse(testInput2)[0].Decode())
	assert.EqualValues(t, 26, partA(testInput))
	assert.EqualValues(t, 5353, partB(testInput2))
	assert.EqualValues(t, 61229, partB(testInput))
	shared.RunDay(partA, day, "a")
	shared.RunDay(partB, day, "b")
}
