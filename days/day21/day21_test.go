package day21

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"strings"
	"testing"
)

type state struct {
	score    int
	position int
}

type player struct {
	score   int
	value   int
	bcounts map[state]int64
	bwins   int64
}

type context struct {
	players []*player
	rounds  int
}

func parse(in string) *context {
	ctx := &context{}
	for _, line := range shared.LinesToStringArray(in) {
		parts := strings.Split(line, " ")
		start := shared.ParseInt(parts[4])
		ctx.players = append(ctx.players, &player{
			score:   0,
			value:   start,
			bcounts: map[state]int64{state{0, 4}: 1},
		})
	}
	return ctx
}

func (c *context) Play() {
	for c.players[0].score < 1000 && c.players[1].score < 1000 {
		// Check which player's turn it is
		p := c.players[c.rounds%2]
		p.value += (c.rounds*3)%100 + 1
		p.value += (c.rounds*3+1)%100 + 1
		p.value += (c.rounds*3+2)%100 + 1
		p.value = (p.value-1)%10 + 1
		p.score += p.value
		c.rounds++
	}
}

var counts = []int64{1, 3, 6, 7, 6, 3, 1}
var amount = []int{3, 4, 5, 6, 7, 8, 9}

// In order to get this function under 1s execution time, I had to change arrays into individual stack variables
// Arrays were causing too much GC activity, reducing performance by 3x-4x
func (c *context) PlayBRec(player int, pos0 int, pos1 int, score0 int, score1 int) (int64, int64) {
	wins0, wins1 := int64(0), int64(0)
	for j := 0; j < len(amount); j++ {
		// Compute new position and score for this dice result
		pos, score := pos0, score0
		if player == 1 {
			pos, score = pos1, score1
		}
		pos = (pos+amount[j]-1)%10 + 1
		score = score + pos
		if score >= 21 {
			// Player has won the round, nothing further to do, just track the wins (all dice combinations)
			if player == 0 {
				wins0 += counts[j]
			} else {
				wins1 += counts[j]
			}
		} else {
			w0p, w1p := int64(0), int64(0)
			if player == 0 {
				w0p, w1p = c.PlayBRec(1, pos, pos1, score, score1)
			} else {
				w0p, w1p = c.PlayBRec(0, pos0, pos, score0, score)
			}
			// Collect all wins for each player, and multiply them by the dice combinations
			wins0 += w0p * counts[j]
			wins1 += w1p * counts[j]
		}
	}
	return wins0, wins1
}

func (c *context) PlayB() int64 {
	w0, _ := c.PlayBRec(0, c.players[0].value, c.players[1].value, 0, 0)
	return w0
}

func (c *context) LowestScore() int {
	return shared.Min(c.players[0].score, c.players[1].score)
}

func (c *context) DieRolls() int {
	return c.rounds * 3
}

func partA(in string) interface{} {
	ctx := parse(in)
	ctx.Play()
	return ctx.LowestScore() * ctx.DieRolls()
}

func partB(in string) interface{} {
	ctx := parse(in)
	return ctx.PlayB()
}

func Test(t *testing.T) {
	day := int64(21)
	testInput := "Player 1 starting position: 4\nPlayer 2 starting position: 8"
	assert.EqualValues(t, 739785, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 444356092776315, partB(testInput))
	shared.RunDay(partB, day, "b")
}
