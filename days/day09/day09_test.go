package day09

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/grid"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"sort"
	"testing"
)

func findMinima(grid *grid.Grid[int]) int {
	return grid.Reduce(0, func(x int, y int, v int, total int) int {
		if v < grid.Get(x-1, y) &&
			v < grid.Get(x+1, y) &&
			v < grid.Get(x, y-1) &&
			v < grid.Get(x, y+1) {
			return total + v + 1
		}
		return total
	})
}

func traceBasin(grid *grid.Grid[int], x int, y int) (int, int) {
	dirs := [][]int{
		{-1, 0},
		{1, 0},
		{0, -1},
		{0, 1},
	}
	for {
		v := grid.Get(x, y)
		if v == 9 {
			return -1, -1
		}
		minValue := 10
		minDir := []int{0, 0}
		for _, dir := range dirs {
			ov := grid.Get(x+dir[0], y+dir[1])
			if ov < minValue && ov < v {
				minValue = ov
				minDir = dir
			}
		}
		// No move possible?
		if minValue == 10 {
			break
		}
		// Move in that direction
		x += minDir[0]
		y += minDir[1]
	}
	return x, y
}

func findBasins(grid *grid.Grid[int]) []int {
	basins := grid.GridFromSize[int](grid.Size())
	grid.Apply(func(x int, y int, v int) {
		bx, by := traceBasin(grid, x, y)
		if bx >= 0 && by >= 0 {
			basins.Inc(bx, by, 1)
		}
	})
	return basins.Linearize()
}

func partA(in string) interface{} {
	grid := grid.IntGridFromString(in)
	grid.SetOutside(10)
	return findMinima(grid)
}

func partB(in string) interface{} {
	grid := grid.IntGridFromString(in)
	grid.SetOutside(10)
	basins := findBasins(grid)
	sort.Sort(sort.Reverse(sort.IntSlice(basins)))
	return basins[0] * basins[1] * basins[2]
}

func Test(t *testing.T) {
	day := int64(9)
	testInput := "2199943210\n3987894921\n9856789892\n8767896789\n9899965678"
	assert.EqualValues(t, 15, partA(testInput))
	assert.EqualValues(t, 1134, partB(testInput))
	shared.RunDay(partA, day, "a")
	shared.RunDay(partB, day, "b")
}
