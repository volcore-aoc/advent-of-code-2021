package day18

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"strings"
	"testing"
)

type pair struct {
	Left    *pair
	Right   *pair
	Literal int
}

func subParsePair(in string) (*pair, string) {
	if in[0] != '[' {
		return &pair{
			Literal: shared.ParseInt(in[0:1]),
		}, in[1:]
	}
	left, rem := subParsePair(in[1:])
	right, rem := subParsePair(rem[1:])
	p := &pair{
		Left:  left,
		Right: right,
	}
	return p, rem[1:]
}

func parsePair(in string) *pair {
	p, _ := subParsePair(strings.TrimSpace(in))
	return p
}

func (p *pair) Magnitude() int {
	if p.Left == nil || p.Right == nil {
		return p.Literal
	}
	return 3*p.Left.Magnitude() + 2*p.Right.Magnitude()
}

func magnitude(in string) int {
	return parsePair(in).Magnitude()
}

func isNumber(r uint8) bool {
	return r >= '0' && r <= '9'
}

func addToFirst(in string, amount int) string {
	for i := range in {
		if !isNumber(in[i]) {
			continue
		}
		// Found a number, extract and replace it
		for j := range in[i:] {
			if !isNumber(in[i+j]) {
				// Found the end
				oldValue := shared.ParseInt(in[i : i+j])
				newValue := oldValue + amount
				return in[:i] + fmt.Sprintf("%d", newValue) + in[i+j:]
			}
		}
	}
	// Found nothing
	return in
}

func addToLast(in string, amount int) string {
	for end := len(in) - 1; end > 0; end-- {
		if !isNumber(in[end]) {
			continue
		}
		for start := end - 1; start > 0; start-- {
			if !isNumber(in[start]) {
				oldValue := shared.ParseInt(in[start+1 : end+1])
				newValue := oldValue + amount
				return in[:start+1] + fmt.Sprintf("%d", newValue) + in[end+1:]
			}
		}
	}
	// Found nothing
	return in
}

func explode(in string, offset int) string {
	rem := strings.SplitN(in[offset:], "]", 2)
	_ = rem
	leftRight := strings.Split(rem[0], ",")
	left := shared.ParseInt(leftRight[0])
	right := shared.ParseInt(leftRight[1])
	// Replace the parts
	pre := addToLast(in[:offset-1], left)
	post := addToFirst(rem[1], right)
	out := pre + "0" + post
	return out
}

func split(in string, offset int) string {
	end := len(in)
	for i := offset; i < len(in); i++ {
		if !isNumber(in[i]) {
			end = i
			break
		}
	}
	value := shared.ParseInt(in[offset:end])
	p := fmt.Sprintf("[%d,%d]", value/2, (value+1)/2)
	return in[:offset] + p + in[end:]
}

func reduce(in string) string {
	// Find explodes
	depth := 0
	for idx, r := range in {
		switch r {
		case '[':
			depth += 1
			if depth > 4 {
				return explode(in, idx+1)
			}
		case ']':
			depth -= 1
		}
	}
	// Find double digit numbers to split
	wasDigit := false
	for idx, r := range in {
		if isNumber(uint8(r)) {
			if wasDigit {
				// Found one!
				return split(in, idx-1)
			}
			wasDigit = true
		} else {
			wasDigit = false
		}
	}
	return in
}

func add(in string, add string) string {
	n := "[" + in + "," + add + "]"
	for {
		on := n
		n = reduce(n)
		if n == on {
			return n
		}
	}
}

func partA(in string) interface{} {
	lines := shared.LinesToStringArray(in)
	current := lines[0]
	for _, line := range lines[1:] {
		current = add(current, line)
	}
	return magnitude(current)
}

func partB(in string) interface{} {
	lines := shared.LinesToStringArray(in)
	max := 0
	for i, lineA := range lines {
		for j, lineB := range lines {
			if i == j {
				continue
			}
			ab := magnitude(add(lineA, lineB))
			ba := magnitude(add(lineA, lineB))
			max = shared.Max(ab, shared.Max(ba, max))
		}
	}
	return max
}

func Test(t *testing.T) {
	day := int64(18)
	testInput := "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n[[[5,[2,8]],4],[5,[[9,9],0]]]\n[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n[[[[5,4],[7,7]],8],[[8,3],8]]\n[[9,3],[[9,9],[6,[4,9]]]]\n[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"
	// Explode tests
	assert.EqualValues(t, "[[[[0,9],2],3],4]", reduce("[[[[[9,8],1],2],3],4]"))
	assert.EqualValues(t, "[7,[6,[5,[7,0]]]]", reduce("[7,[6,[5,[4,[3,2]]]]]"))
	assert.EqualValues(t, "[[6,[5,[7,0]]],3]", reduce("[[6,[5,[4,[3,2]]]],1]"))
	assert.EqualValues(t, "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", reduce("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"))
	assert.EqualValues(t, "[[3,[2,[8,0]]],[9,[5,[7,0]]]]", reduce("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"))
	// Split tests
	assert.EqualValues(t, "[5,5]", reduce("10"))
	assert.EqualValues(t, "[5,6]", reduce("11"))
	assert.EqualValues(t, "[6,6]", reduce("12"))
	// magnitude tests
	assert.EqualValues(t, 29, magnitude("[9,1]"))
	assert.EqualValues(t, 143, magnitude("[[1,2],[[3,4],5]]"))
	assert.EqualValues(t, 1384, magnitude("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"))
	assert.EqualValues(t, 445, magnitude("[[[[1,1],[2,2]],[3,3]],[4,4]]"))
	assert.EqualValues(t, 791, magnitude("[[[[3,0],[5,3]],[4,4]],[5,5]]"))
	assert.EqualValues(t, 1137, magnitude("[[[[5,0],[7,4]],[5,5]],[6,6]]"))
	assert.EqualValues(t, 3488, magnitude("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"))
	// Add tests
	assert.EqualValues(t, "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", add("[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]"))
	assert.EqualValues(t, 4140, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 3993, partB(testInput))
	shared.RunDay(partB, day, "b")
}
