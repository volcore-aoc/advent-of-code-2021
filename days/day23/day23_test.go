package day23

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

type State = string

type context struct {
	state State
}

func parse(in string) *context {
	lines := shared.LinesToStringArray(in)
	state := lines[1][1:12] +
		lines[2][3:4] + lines[2][5:6] + lines[2][7:8] + lines[2][9:10] +
		lines[3][3:4] + lines[3][5:6] + lines[3][7:8] + lines[3][9:10]
	return &context{
		state: state,
	}
}

func isBState(s State) bool {
	return len(s) != 19
}

func targetState(s State) State {
	if isBState(s) {
		return "...........ABCDABCDABCDABCD"
	} else {
		return "...........ABCDABCD"
	}
}

var cost = map[uint8]int{
	'A': 1,
	'B': 10,
	'C': 100,
	'D': 1000,
}

var target = map[uint8]int{
	'A': 11,
	'B': 12,
	'C': 13,
	'D': 14,
}

var valid = map[int]uint8{
	11: 'A',
	12: 'B',
	13: 'C',
	14: 'D',
	15: 'A',
	16: 'B',
	17: 'C',
	18: 'D',
	19: 'A',
	20: 'B',
	21: 'C',
	22: 'D',
	23: 'A',
	24: 'B',
	25: 'C',
	26: 'D',
}

var topToHallway = map[int]int{
	11: 2,
	12: 4,
	13: 6,
	14: 8,
}

// 0  1  2  3  4  5  6  7  8  9  10
//      11    12    13    14
//      15    16    17    18
//      19    20    21    22
//      23    24    25    26

var top = []int{11, 12, 13, 14}
var hallway = []int{0, 1, 3, 5, 7, 9, 10}

func in(r uint8, l ...uint8) bool {
	for _, c := range l {
		if r == c {
			return true
		}
	}
	return false
}

func ordered(i int, j int) (int, int) {
	if i < j {
		return i, j
	} else {
		return j, i
	}
}

func move(s State, i int, j int) State {
	i, j = ordered(i, j)
	return s[:i] + s[j:j+1] + s[i+1:j] + s[i:i+1] + s[j+1:]
}

func clearHallwayPath(s State, i int, j int, self int) bool {
	i, j = ordered(i, j)
	for k := i; k <= j; k++ {
		if k == self {
			continue
		}
		if s[k] != '.' {
			return false
		}
	}
	return true
}

func hallwaySteps(i int, j int) int {
	i, j = ordered(i, j)
	return j - i
}

func validColumn(s State, i int) bool {
	t := valid[i]
	return in(s[i], t, '.') &&
		in(s[i+4], t, '.') &&
		in(s[i+8], t, '.') &&
		in(s[i+12], t, '.')
}

func validOrEmptyBelow(s State, i int) bool {
	t := valid[i]
	for i < len(s) {
		if !in(s[i], t, '.') {
			return false
		}
		i += 4
	}
	return true
}

func iterateMovesB(s State, f func(s State, cost int)) {
	// check move down (top to bottom if right slot)
	for _, i := range top {
		// check if this column contains any invalids. if yes, skip pushing things down
		if !validColumn(s, i) {
			continue
		}
		// No invalids, so push things down if possible
		for j := 0; j < 3; j++ {
			idx := i + j*4
			// Skip if empty
			if in(s[idx], '.') {
				continue
			}
			// Skip if below is not empty
			if s[idx+4] != '.' {
				continue
			}
			// Bottom is empty and we're in the right slot, so move down
			f(move(s, idx, idx+4), cost[s[idx]])
		}
	}
	// check move up (bottom to top if not right slot)
	for _, i := range top {
		// This column contains invalid, so push things up (note that this might move valids up as well, but we ignore
		// for now)
		for j := 1; j < 4; j++ {
			idx := i + j*4
			// Skip if only valid or empty below
			if validOrEmptyBelow(s, idx) {
				break
			}
			// Skip if empty
			if in(s[idx], '.') {
				continue
			}
			// Skip if top is blocked
			if !in(s[idx-4], '.') {
				continue
			}
			// Move
			f(move(s, idx, idx-4), cost[s[idx]])
		}
	}
	// check move in (hallway to right slot if free and valid on bottom slot)
	for _, i := range hallway {
		if s[i] == '.' {
			continue
		}
		// Check if target is available and this wouldn't block anything
		j := target[s[i]]
		if !validOrEmptyBelow(s, j) {
			// Can't move in yet, there is something in the way
			continue
		}
		// Make sure path is clear
		h := topToHallway[j]
		if !clearHallwayPath(s, i, h, i) {
			continue
		}
		// Found a valid spot
		f(move(s, i, j), cost[s[i]]*(1+hallwaySteps(i, h)))
	}
	// check move out (top to hallway if not right slot if path)
	for _, i := range top {
		if s[i] == '.' {
			continue
		}
		// Are we in the right slot, and there is nothing bad below us?
		if validOrEmptyBelow(s, i) {
			// Does not need to move out of the way
			continue
		}
		// We need to move out, where?
		for _, j := range hallway {
			// Is it occupied?
			if s[j] != '.' {
				continue
			}
			// Is there a valid, unoccupied path?
			h := topToHallway[i]
			if !clearHallwayPath(s, h, j, -1) {
				continue
			}
			f(move(s, i, j), cost[s[i]]*(1+hallwaySteps(h, j)))
		}
	}
}
func iterateMoves(s State, f func(s State, cost int)) {
	// check move down (top to bottom if right slot)
	for _, i := range top {
		// Skip if empty
		if in(s[i], '.') {
			continue
		}
		// Skip if not in the right column
		if !in(s[i], valid[i]) {
			continue
		}
		// Skip if below is not empty
		if s[i+4] != '.' {
			continue
		}
		// Bottom is empty and we're in the right slot, so move down
		f(move(s, i, i+4), cost[s[i]])
	}
	// check move up (bottom to top if not right slot)
	for _, i := range top {
		// Skip if empty
		if in(s[i+4], '.') {
			continue
		}
		// Skip if already in the right place
		if in(s[i+4], valid[i+4]) {
			continue
		}
		// Skip if top is blocked
		if !in(s[i], '.') {
			continue
		}
		// Move
		f(move(s, i, i+4), cost[s[i+4]])
	}
	// check move in (hallway to right slot if free and valid on bottom slot)
	for _, i := range hallway {
		if s[i] == '.' {
			continue
		}
		// Check if target is available and this wouldn't block anything
		j := target[s[i]]
		if s[j] != '.' || !in(s[j+4], valid[j+4], '.') {
			continue
		}
		// Make sure path is clear
		h := topToHallway[j]
		if !clearHallwayPath(s, i, h, i) {
			continue
		}
		// Found a valid spot
		f(move(s, i, j), cost[s[i]]*(1+hallwaySteps(i, h)))
	}
	// check move out (top to hallway if not right slot if path)
	for _, i := range top {
		if s[i] == '.' {
			continue
		}
		// Are we in the right slot?
		if s[i] == valid[i] {
			// Right slot, but is the one below us as well?
			if in(s[i+4], valid[i+4], '.') {
				// Does not need to move out of the way
				continue
			}
		}
		// We need to move out, where?
		for _, j := range hallway {
			// Is it occupied?
			if s[j] != '.' {
				continue
			}
			// Is there a valid, unoccupied path?
			h := topToHallway[i]
			if !clearHallwayPath(s, h, j, -1) {
				continue
			}
			f(move(s, i, j), cost[s[i]]*(1+hallwaySteps(h, j)))
		}
	}
}

func dump(s State) {
	fmt.Printf("#############\n")
	fmt.Printf("#%s#\n", s[:11])
	fmt.Printf("###%c#%c#%c#%c###\n", s[11], s[12], s[13], s[14])
	fmt.Printf("  #%c#%c#%c#%c#\n", s[15], s[16], s[17], s[18])
	if isBState(s) {
		fmt.Printf("  #%c#%c#%c#%c#\n", s[19], s[20], s[21], s[22])
		fmt.Printf("  #%c#%c#%c#%c#\n", s[23], s[24], s[25], s[26])
	}
	fmt.Printf("  #########\n")
}

func search(start State) int {
	costs := map[State]int{start: 0}
	prev := map[State]State{}
	queue := []State{start}
	for len(queue) > 0 {
		// Pop
		s := queue[0]
		queue = queue[1:]
		//dump(s)
		// Fetch the current cost
		oldCost := costs[s]
		handler := func(next State, cost int) {
			// Compute the cost of getting here
			newCost := cost + oldCost
			// Check if we've been here with at most the same cost
			if v, ok := costs[next]; ok && v <= newCost {
				return
			}
			// First time here at the cost, so queue
			costs[next] = oldCost + cost
			prev[next] = s
			queue = append(queue, next)
		}
		if isBState(s) {
			iterateMovesB(s, handler)
		} else {
			iterateMoves(s, handler)
		}
	}
	//// Track back -- for debugging
	//cur := targetState(start)
	//for cur != "" {
	//	fmt.Printf("cost: %d\n", costs[cur])
	//	dump(cur)
	//	cur = prev[cur]
	//}
	return costs[targetState(start)]
}

func partA(in string) interface{} {
	ctx := parse(in)
	return search(ctx.state)
}

func partB(in string) interface{} {
	ctx := parse(in)
	ctx.state = ctx.state[0:15] + "DCBADBAC" + ctx.state[15:]
	return search(ctx.state)
}

func Test(t *testing.T) {
	day := int64(23)
	testInput := "#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########"
	assert.EqualValues(t, 12521, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 44169, partB(testInput))
	shared.RunDay(partB, day, "b")
}
