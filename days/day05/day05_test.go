package day05

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"gitlab.com/volcore-aoc/advent-of-code-2021/vec"
	"strings"
	"testing"
)

// Coordinates seem to only go from 0 to 1000, so we can use that to bound our map to linearize the indices
const day05MaxCoord = 1000

type day05Line struct {
	Start *vec.Vec2[int64]
	End   *vec.Vec2[int64]
}

type day05Map struct {
	Lines []*day05Line
	Map   map[int64]int64
}

func day05Parse(in string) *day05Map {
	lines := shared.LinesToStringArray(in)
	var out []*day05Line
	for _, line := range lines {
		leftRight := strings.Split(line, " -> ")
		l := &day05Line{
			Start: vec.Vec2i64FromCommaList(leftRight[0]),
			End:   vec.Vec2i64FromCommaList(leftRight[1]),
		}
		out = append(out, l)
	}
	return &day05Map{
		Lines: out,
		Map:   map[int64]int64{},
	}
}

func (m *day05Map) Coord2Idx(p *vec.Vec2[int64]) int64 {
	return p.X + p.Y*day05MaxCoord
}

func (m *day05Map) Idx2Coord(idx int64) *vec.Vec2[int64] {
	return vec.V2[int64](idx%day05MaxCoord, idx/day05MaxCoord)
}

func (m *day05Map) Apply(diag bool) {
	for _, l := range m.Lines {
		isAligned := l.Start.X == l.End.X || l.Start.Y == l.End.Y
		if !diag && !isAligned {
			continue
		}
		dir := l.End.Sub(l.Start)
		if isAligned {
			dir = dir.Divide(dir.L1())
		} else {
			dir = dir.Divide(dir.L1() / 2)
		}
		p := l.Start
		for !p.IsEqual(l.End) {
			m.Map[m.Coord2Idx(p)] += 1
			p = p.Add(dir)
		}
		m.Map[m.Coord2Idx(p)] += 1
	}
}

func (m *day05Map) CountAtLeast(n int64) int64 {
	count := int64(0)
	for _, v := range m.Map {
		if v >= n {
			count++
		}
	}
	return count
}

func day05a(in string) interface{} {
	m := day05Parse(in)
	m.Apply(false)
	return m.CountAtLeast(2)
}

func day05b(in string) interface{} {
	m := day05Parse(in)
	m.Apply(true)
	return m.CountAtLeast(2)
}

func TestDay05(t *testing.T) {
	day := int64(5)
	aFunc := day05a
	bFunc := day05b
	testInput := "0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2"
	assert.EqualValues(t, 5, aFunc(testInput))
	assert.EqualValues(t, 12, bFunc(testInput))
	shared.RunDay(aFunc, day, "a")
	shared.RunDay(bFunc, day, "b")
}
