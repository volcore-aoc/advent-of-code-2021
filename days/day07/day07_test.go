package day07

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

func computeDistanceA(crabs []int, position int) int {
	crabs = shared.ArrayMap(crabs, func(x int) int { return shared.Abs(x - position) })
	return shared.ArraySum(crabs)
}

func computeDistanceB(crabs []int, position int) int {
	crabs = shared.ArrayMap(crabs, func(x int) int { return shared.Abs(x - position) })
	crabs = shared.ArrayMap(crabs, func(x int) int { return x * (x + 1) / 2 })
	return shared.ArraySum(crabs)
}

func compute(in string, f func([]int, int) int) int {
	crabs := shared.CommaListToIntArray(in)
	min, max := shared.ArrayMinMax(crabs)
	options := shared.ArrayRange(min, max)
	options = shared.ArrayMap(options, func(x int) int { return f(crabs, x) })
	minValue, _ := shared.ArrayMinMax(options)
	return minValue
}

func partA(in string) interface{} {
	return compute(in, computeDistanceA)
}

func partB(in string) interface{} {
	return compute(in, computeDistanceB)
}

func Test(t *testing.T) {
	day := int64(7)
	testInput := "16,1,2,0,4,2,7,1,2,14"
	crabs := shared.CommaListToIntArray(testInput)
	assert.Equal(t, 41, computeDistanceA(crabs, 1))
	assert.Equal(t, 39, computeDistanceA(crabs, 3))
	assert.Equal(t, 71, computeDistanceA(crabs, 10))
	assert.EqualValues(t, 37, partA(testInput))
	assert.Equal(t, 206, computeDistanceB(crabs, 2))
	assert.Equal(t, 168, computeDistanceB(crabs, 5))
	assert.EqualValues(t, 168, partB(testInput))
	shared.RunDay(partA, day, "a")
	shared.RunDay(partB, day, "b")
}
