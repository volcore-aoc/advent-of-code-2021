package day03

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

func day03a(in string) interface{} {
	lines := shared.LinesToStringArray(in)
	// Count the numbers
	counter1 := make([]int, len(lines[0]))
	for _, line := range lines {
		for i, c := range line {
			if c == '1' {
				counter1[i] += 1
			}
		}
	}
	// Extract gamma and epsilon
	numLines := len(lines)
	gamma := 0
	epsilon := 0
	for _, n := range counter1 {
		gamma = gamma << 1
		epsilon = epsilon << 1
		if n > numLines-n {
			gamma++
		} else {
			epsilon++
		}
	}
	return gamma * epsilon
}

func countPosition(pos int, lines []string) int {
	counter := 0
	for _, line := range lines {
		if line[pos] == '1' {
			counter++
		}
	}
	return counter
}

func mostCommon(pos int, lines []string) uint8 {
	count := countPosition(pos, lines)
	if count >= len(lines)-count {
		return '1'
	} else {
		return '0'
	}
}

func leastCommon(pos int, lines []string) uint8 {
	count := countPosition(pos, lines)
	if count >= len(lines)-count {
		return '0'
	} else {
		return '1'
	}
}

func filterPosition(pos int, value uint8, lines []string) []string {
	var outList []string
	for _, line := range lines {
		if line[pos] == value {
			outList = append(outList, line)
		}
	}
	return outList
}

func day03b(in string) interface{} {
	lines := shared.LinesToStringArray(in)
	oxygenLines := lines
	for i := 0; i < len(lines[0]) && len(oxygenLines) > 1; i++ {
		filter := mostCommon(i, oxygenLines)
		oxygenLines = filterPosition(i, filter, oxygenLines)
	}
	oxygen := shared.ParseBin(oxygenLines[0])
	co2Lines := lines
	for i := 0; i < len(lines[0]) && len(co2Lines) > 1; i++ {
		filter := leastCommon(i, co2Lines)
		co2Lines = filterPosition(i, filter, co2Lines)
	}
	co2 := shared.ParseBin(co2Lines[0])
	return oxygen * co2
}

func TestDay03(t *testing.T) {
	assert.EqualValues(t, 198, day03a("00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010"))
	shared.RunDay(day03a, 3, "a")
	assert.EqualValues(t, 230, day03b("00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010"))
	shared.RunDay(day03b, 3, "b")
}
