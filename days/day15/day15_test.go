package day15

import (
	"github.com/beefsack/go-astar"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/grid"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"gitlab.com/volcore-aoc/advent-of-code-2021/vec"
	"testing"
)

type context struct {
	grid  *grid.Grid[int]
	tiles []*tile
}

type tile struct {
	Pos *vec.Vec2i
	ctx *context
}

func (c *context) Tile(x int, y int) *tile {
	w, h := c.grid.Size()
	if x < 0 || y < 0 || x >= w || y >= h {
		return nil
	}
	return c.tiles[x+y*w]
}

func (t *tile) PathNeighbors() []astar.Pather {
	var n []astar.Pather
	np := []*vec.Vec2i{
		t.Pos.AddXY(0, 1),
		t.Pos.AddXY(0, -1),
		t.Pos.AddXY(1, 0),
		t.Pos.AddXY(-1, 0),
	}
	for _, pos := range np {
		t := t.ctx.Tile(pos.X, pos.Y)
		if t != nil {
			n = append(n, t)
		}
	}
	return n
}

func (t *tile) PathNeighborCost(to astar.Pather) float64 {
	tp := to.(*tile).Pos
	return float64(t.ctx.grid.Get(tp.X, tp.Y))
}

func (t *tile) PathEstimatedCost(to astar.Pather) float64 {
	tp := to.(*tile).Pos
	return float64(t.Pos.Sub(tp).L1())
}

func (c *context) FindShortestPath() int {
	start := c.Tile(0, 0)
	w, h := c.grid.Size()
	end := c.Tile(w-1, h-1)
	_, distance, found := astar.Path(start, end)
	if !found {
		log.Errorf("Could not find path")
	}
	return int(distance)
}

func parse(in string, repeat int) *context {
	ctx := &context{
		grid: grid.IntGridFromString(in),
	}
	if repeat > 1 {
		ow, oh := ctx.grid.Size()
		ctx.grid.Repeat(repeat, repeat)
		ctx.grid.Map(func(x int, y int, v int) int {
			i := x / ow
			j := y / oh
			return (v+i+j-1)%9 + 1
		})
	}
	w, h := ctx.grid.Size()
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			ctx.tiles = append(ctx.tiles, &tile{
				Pos: vec.V2[int](x, y),
				ctx: ctx,
			})
		}
	}
	return ctx
}

func partA(in string) interface{} {
	ctx := parse(in, 1)
	return ctx.FindShortestPath()
}

func partB(in string) interface{} {
	ctx := parse(in, 5)
	return ctx.FindShortestPath()
}

func Test(t *testing.T) {
	day := int64(15)
	testInput := "1163751742\n1381373672\n2136511328\n3694931569\n7463417111\n1319128137\n1359912421\n3125421639\n1293138521\n2311944581"
	assert.EqualValues(t, 40, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 315, partB(testInput))
	shared.RunDay(partB, day, "b")
}
