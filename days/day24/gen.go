package day24

var EqlLUT = map[bool]int{true: 1, false: 0}

func run(inp []int) int {
	var x, y, z, w int
	_, _, _, _ = x, y, z, w
	// inp w
	w = inp[0]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 15
	x = x + 15
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 15
	y = y + 15
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[1]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 15
	x = x + 15
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 10
	y = y + 10
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[2]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 12
	x = x + 12
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 2
	y = y + 2
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[3]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 13
	x = x + 13
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 16
	y = y + 16
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[4]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -12
	x = x + -12
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 12
	y = y + 12
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[5]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 10
	x = x + 10
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 11
	y = y + 11
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[6]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -9
	x = x + -9
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 5
	y = y + 5
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[7]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 14
	x = x + 14
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 16
	y = y + 16
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[8]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 1
	z = z / 1
	// add x 13
	x = x + 13
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 6
	y = y + 6
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[9]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -14
	x = x + -14
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 15
	y = y + 15
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[10]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -11
	x = x + -11
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 3
	y = y + 3
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[11]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -2
	x = x + -2
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 12
	y = y + 12
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[12]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -16
	x = x + -16
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 10
	y = y + 10
	// mul y x
	y = y * x
	// add z y
	z = z + y
	// inp w
	w = inp[13]
	// mul x 0
	x = x * 0
	// add x z
	x = x + z
	// mod x 26
	x = x % 26
	// div z 26
	z = z / 26
	// add x -14
	x = x + -14
	// eql x w
	x = EqlLUT[x == w]
	// eql x 0
	x = EqlLUT[x == 0]
	// mul y 0
	y = y * 0
	// add y 25
	y = y + 25
	// mul y x
	y = y * x
	// add y 1
	y = y + 1
	// mul z y
	z = z * y
	// mul y 0
	y = y * 0
	// add y w
	y = y + w
	// add y 13
	y = y + 13
	// mul y x
	y = y * x
	// add z y
	z = z + y
	return z
}
