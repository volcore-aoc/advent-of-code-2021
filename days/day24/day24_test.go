package day24

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"io/ioutil"
	"math/rand"
	"strings"
	"testing"
)

func codegen(in string) {
	var sb strings.Builder
	sb.WriteString("package day24\n\n")
	sb.WriteString("\n")
	sb.WriteString("var EqlLUT = map[bool]int{ true:  1, false: 0}\n")
	sb.WriteString("\n")
	sb.WriteString("func run(inp []int) int {\n")
	sb.WriteString("  var x, y, z, w int\n")
	sb.WriteString("  _, _, _, _ = x, y, z, w\n")
	inpIdx := 0
	for _, line := range shared.LinesToStringArray(in) {
		parts := strings.Split(line, " ")
		sb.WriteString(fmt.Sprintf("  // %s\n", line))
		switch parts[0] {
		case "add":
			sb.WriteString(fmt.Sprintf("  %s = %s + %s\n", parts[1], parts[1], parts[2]))
		case "inp":
			sb.WriteString(fmt.Sprintf("  %s = inp[%d]\n", parts[1], inpIdx))
			inpIdx++
		case "mul":
			sb.WriteString(fmt.Sprintf("  %s = %s * %s\n", parts[1], parts[1], parts[2]))
		case "mod":
			sb.WriteString(fmt.Sprintf("  %s = %s %% %s\n", parts[1], parts[1], parts[2]))
		case "div":
			sb.WriteString(fmt.Sprintf("  %s = %s / %s\n", parts[1], parts[1], parts[2]))
		case "eql":
			sb.WriteString(fmt.Sprintf("  %s = EqlLUT[%s == %s]\n", parts[1], parts[1], parts[2]))
		default:
			sb.WriteString("  // TODO\n")
		}
	}
	sb.WriteString("  return z\n")
	sb.WriteString("}\n")
	ioutil.WriteFile("../../days/day24/gen.go", []byte(sb.String()), 0644)
}

func partA(in string) interface{} {
	codegen(in)
	return 0
}

func construct2(w0, w2, w3, w5, w7, w9, w12 int) []int {
	return []int{
		w0,
		w12 + 6,
		w2,
		w3,
		w3 + 4,
		w5,
		w5 + 2,
		w7,
		w9 + 8,
		w9,
		w7 + 5,
		w2,
		w12,
		w0 + 1,
	}
}

func construct() []int {
	w0 := rand.Intn(9-1) + 1
	w2 := rand.Intn(9) + 1
	w3 := rand.Intn(9-4) + 1
	w5 := rand.Intn(9-2) + 1
	w7 := rand.Intn(9-5) + 1
	w9 := rand.Intn(9-8) + 1
	w12 := rand.Intn(9-6) + 1
	return construct2(w0, w2, w3, w5, w7, w9, w12)
}

func Test(t *testing.T) {
	// Just for codegen
	//shared.RunDay(partA, 24, "a")
	for i := 1; i < 100000; i++ {
		mn := construct()
		assert.EqualValues(t, run2(mn), run(mn))
	}
	max := construct2(8, 9, 5, 7, 4, 1, 3)
	log.WithField("max", max).Info("part a")
	min := construct2(1, 1, 1, 1, 1, 1, 1)
	log.WithField("min", min).Info("part b")
}
