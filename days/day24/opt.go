package day24

func run2(inp []int) int {
	var x, z int
	// inp w
	w0 := inp[0]
	z = w0 + 15

	// inp w
	w1 := inp[1]
	z = z*26 + (w1 + 10)

	// inp2
	w2 := inp[2]
	z = z*26 + (w2 + 2)

	// inp3
	w3 := inp[3]
	z = z*26 + (w3 + 16)

	// inp4
	w4 := inp[4]
	z = z / 26
	if w4 != w3+4 {
		z = z*26 + (w4 + 12)
	}

	// z0 = w0 + 15
	// z1 = (w0+15)*26 + (w1 + 10)
	// z2 = (w0+15)*26*26 + (w1+10)*26 + (w2 + 2)
	// z3 = (w0+15)*26*26*26 + (w1+10)*26*26 + (w2+2)*26 + (w3 + 16)
	// z4 = (w0+15)*26*26 + (w1+10)*26 + (w2 + 2)
	// -or- z4 = (w0+15)*26*26*26 + (w1+10)*26*26 + (w2+2)*26 + (w4 + 12) -- if w4 == w3+4

	// inp5
	w5 := inp[5]
	z = z*26 + (w5 + 11)

	// inp6
	w6 := inp[6]
	z = z / 26
	if w6 != w5+2 {
		z = z*26 + (w6 + 5)
	}

	// inp7
	w7 := inp[7]
	x = z%26 + 14
	x = 1 - EqlLUT[x == w7]
	z = (z/1)*(25*x+1) + (w7+16)*x

	// inp8
	w8 := inp[8]
	x = z%26 + 13
	x = 1 - EqlLUT[x == w8]
	z = (z/1)*(25*x+1) + (w8+6)*x

	// inp9
	w9 := inp[9]
	z = z / 26
	if w9+8 != w8 {
		z = z*26 + (w9 + 15)
	}

	// inp10
	w10 := inp[10]
	if w9+8 == w8 {
		z = z / 26
		if w10 != w7+5 {
			z = z*26 + (w10 + 3)
		}
	} else {
		x = z%26 - 11
		x = 1 - EqlLUT[x == w10]
		z = (z/26)*(25*x+1) + (w10+3)*x
	}

	// inp11
	w11 := inp[11]
	x = z%26 - 2
	x = 1 - EqlLUT[x == w11]
	z = (z/26)*(25*x+1) + (w11+12)*x

	// inp12
	w12 := inp[12]
	x = z%26 - 16
	x = 1 - EqlLUT[x == w12]
	z = (z/26)*(25*x+1) + (w12+10)*x

	// inp13
	w13 := inp[13]
	x = z%26 - 14
	x = 1 - EqlLUT[x == w13]
	z = (z/26)*(25*x+1) + (w13+13)*x

	// w4 == w3+4
	// w6 == w5+2
	// w9+8 == w8
	// w10 == w7+5
	// w11 == w2
	// w12 == w1 + 6
	// w13 == w0 + 1

	return z
}
