package day01

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

func day01a(in string) interface{} {
	m := shared.LinesToInt64Array(in)
	last := m[0]
	incs := int64(0)
	for _, v := range m {
		if v > last {
			incs++
		}
		last = v
	}
	return incs
}

func day01b(in string) interface{} {
	m := shared.LinesToInt64Array(in)
	window := m[0] + m[1] + m[2]
	incs := int64(0)
	for i := 3; i < len(m); i++ {
		newWindow := window + m[i] - m[i-3]
		if newWindow > window {
			incs++
		}
		window = newWindow
	}
	return incs
}

func TestDay01(t *testing.T) {
	assert.EqualValues(t, 7, day01a("199\n200\n208\n210\n200\n207\n240\n269\n260\n263"))
	shared.RunDay(day01a, 1, "a")
	assert.EqualValues(t, 5, day01b("199\n200\n208\n210\n200\n207\n240\n269\n260\n263"))
	shared.RunDay(day01b, 1, "b")
}
