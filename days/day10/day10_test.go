package day10

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"sort"
	"testing"
)

// Time for generics in go!

type CharStack struct {
	runes []rune
}

func (s *CharStack) Push(r rune) {
	s.runes = append(s.runes, r)
}

func (s *CharStack) Pop() rune {
	r := s.runes[len(s.runes)-1]
	s.runes = s.runes[:len(s.runes)-1]
	return r
}

func (s *CharStack) Empty() bool {
	return len(s.runes) == 0
}

var costs = map[rune]int{
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137,
}

var points = map[rune]int{
	'(': 1,
	'[': 2,
	'{': 3,
	'<': 4,
}

var expects = map[rune]rune{
	')': '(',
	']': '[',
	'}': '{',
	'>': '<',
}

func corruptedAmount(line string) int {
	stack := &CharStack{}
	for _, r := range line {
		expect, ok := expects[r]
		if !ok {
			stack.Push(r)
		} else {
			ref := stack.Pop()
			if expect != ref {
				return costs[r]
			}
		}
	}
	return 0
}

func partA(in string) interface{} {
	lines := shared.LinesToStringArray(in)
	sum := 0
	for _, line := range lines {
		sum += corruptedAmount(line)
	}
	return sum
}

func partB(in string) interface{} {
	lines := shared.LinesToStringArray(in)
	var filteredLines = []string{}
	for _, line := range lines {
		if corruptedAmount(line) > 0 {
			continue
		}
		filteredLines = append(filteredLines, line)
	}
	scores := []int{}
	for _, line := range filteredLines {
		stack := &CharStack{}
		for _, r := range line {
			_, ok := expects[r]
			if !ok {
				stack.Push(r)
			} else {
				stack.Pop()
			}
		}
		// Now sum up the results
		score := 0
		for !stack.Empty() {
			score *= 5
			r := stack.Pop()
			score += points[r]
		}
		scores = append(scores, score)
	}
	// Sort
	sort.Ints(scores)
	return scores[len(scores)/2]
}

func Test(t *testing.T) {
	day := int64(10)
	testInput := "[({(<(())[]>[[{[]{<()<>>\n[(()[<>])]({[<{<<[]>>(\n{([(<{}[<>[]}>{[]{[(<()>\n(((({<>}<{<{<>}{[]{[]{}\n[[<[([]))<([[{}[[()]]]\n[{[{({}]{}}([{[{{{}}([]\n{<[[]]>}<{[{[{[]{()[[[]\n[<(<(<(<{}))><([]([]()\n<{([([[(<>()){}]>(<<{{\n<{([{{}}[<[[[<>{}]]]>[]]"
	assert.EqualValues(t, 26397, partA(testInput))
	assert.EqualValues(t, 288957, partB(testInput))
	shared.RunDay(partA, day, "a")
	shared.RunDay(partB, day, "b")
}
