package day17

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"gitlab.com/volcore-aoc/advent-of-code-2021/vec"
	"strings"
	"testing"
)

type context struct {
	XBounds *vec.Vec2i
	YBounds *vec.Vec2i
}

func parse(in string) *context {
	parts := strings.Split(strings.TrimSpace(in), " ")
	xs := strings.Split(parts[2][2:len(parts[2])-1], "..")
	ys := strings.Split(parts[3][2:], "..")
	ctx := &context{
		XBounds: vec.Vec2iFromStringArray(xs),
		YBounds: vec.Vec2iFromStringArray(ys),
	}
	return ctx
}

func (c *context) Trace(dir *vec.Vec2i) int {
	return c.Trace2(dir, vec.V2(-1, -1))
}

func (c *context) Trace2(dir *vec.Vec2i, acc *vec.Vec2i) int {
	pos := vec.V2[int](0, 0)
	maxY := 0
	for pos.X <= c.XBounds.Y && pos.Y >= c.YBounds.X {
		pos = pos.Add(dir)
		maxY = shared.Max(pos.Y, maxY)
		dir.X = shared.Max(dir.X+acc.X, 0)
		dir.Y = dir.Y + acc.Y
		// Early out if we wont be able to reach far enough
		if dir.X <= 0 && pos.X < c.XBounds.X {
			return -1
		}
		// Detect success
		if pos.X >= c.XBounds.X && pos.X <= c.XBounds.Y && pos.Y >= c.YBounds.X && pos.Y <= c.YBounds.Y {
			return maxY
		}
	}
	return -1
}

func (c *context) FindXs() []int {
	xs := []int{}
	c2 := context{c.XBounds, vec.V2(0, 0)}
	for x := 1; x <= c.XBounds.Y; x++ {
		if c2.Trace2(vec.V2(x, 0), vec.V2(-1, 0)) >= 0 {
			xs = append(xs, x)
		}
	}
	return xs
}

func (c *context) FindYs() []int {
	ys := []int{}
	c2 := context{vec.V2(0, 0), c.YBounds}
	for y := c.YBounds.X; y <= -c.YBounds.X; y++ {
		if c2.Trace2(vec.V2(0, y), vec.V2(0, -1)) >= 0 {
			ys = append(ys, y)
		}
	}
	return ys
}

func partA(in string) interface{} {
	ctx := parse(in)
	// Find all xs
	xs := ctx.FindXs()
	ys := ctx.FindYs()
	// Test all candidates
	maxY := 0
	for _, y := range ys {
		for _, x := range xs {
			maxY = shared.Max(maxY, ctx.Trace(vec.V2(x, y)))
		}
	}
	return maxY
}

func partB(in string) interface{} {
	ctx := parse(in)
	// Find all xs
	xs := ctx.FindXs()
	ys := ctx.FindYs()
	// Test all candidates
	count := 0
	for _, y := range ys {
		for _, x := range xs {
			if ctx.Trace(vec.V2(x, y)) >= 0 {
				count++
			}
		}
	}
	return count
}

func Test(t *testing.T) {
	day := int64(17)
	testInput := "target area: x=20..30, y=-10..-5"
	assert.EqualValues(t, 3, parse(testInput).Trace(vec.V2(7, 2)))
	assert.EqualValues(t, 6, parse(testInput).Trace(vec.V2(6, 3)))
	assert.EqualValues(t, 0, parse(testInput).Trace(vec.V2(9, 0)))
	assert.EqualValues(t, -1, parse(testInput).Trace(vec.V2(17, -4)))
	assert.EqualValues(t, 45, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 112, partB(testInput))
	shared.RunDay(partB, day, "b")
}
