package day11

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/grid"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

type context struct {
	grid *grid.Grid[int]
}

func (c *context) step() int {
	w, h := c.grid.Size()
	// Increase all by one
	c.grid.Map(func(x int, y int, v int) int {
		return v + 1
	})
	// Flash until convergence
	flashed := grid.GridFromSize[int](w, h)
	cont := true
	for cont {
		cont = false
		c.grid.Apply(func(x int, y int, v int) {
			if v > 9 && flashed.Get(x, y) == 0 {
				c.grid.SplatInc(x, y, 1, 1, 1)
				flashed.Set(x, y, 1)
				cont = true
			}
		})
	}
	// Reset all the flashed
	flashed.Apply(func(x int, y int, v int) {
		if v > 0 {
			c.grid.Set(x, y, 0)
		}
	})
	return flashed.Sum()
}

func (c *context) steps(count int) int {
	flashes := 0
	for i := 0; i < count; i++ {
		flashes += c.step()
	}
	return flashes
}

func (c *context) findSimStep() int {
	targetCount := c.grid.NumCells()
	step := 0
	for {
		step++
		if c.step() == targetCount {
			return step
		}
	}
}

func partA(in string) interface{} {
	context := &context{grid: grid.IntGridFromString(in)}
	flashes := context.steps(100)
	return flashes
}

func partB(in string) interface{} {
	context := &context{grid: grid.IntGridFromString(in)}
	step := context.findSimStep()
	return step
}

func Test(t *testing.T) {
	day := int64(11)
	testInput := "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526"
	assert.EqualValues(t, 1656, partA(testInput))
	assert.EqualValues(t, 195, partB(testInput))
	shared.RunDay(partA, day, "a")
	shared.RunDay(partB, day, "b")
}
