package day19

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/set"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"gitlab.com/volcore-aoc/advent-of-code-2021/vec"
	"regexp"
	"strings"
	"testing"
)

type scanner struct {
	Index   int
	Beacons Beacons
	Align   *alignment
}

type Beacons set.Set[vec.Vec3i]

type context struct {
	Scanners []*scanner
}

type alignment struct {
	translation *vec.Vec3i
	orientation int
}

func (a *alignment) String() string {
	return fmt.Sprintf("%v %v", a.translation, a.orientation)
}

var Xp = vec.V3i(1, 0, 0)
var Xm = vec.V3i(-1, 0, 0)
var Yp = vec.V3i(0, 1, 0)
var Ym = vec.V3i(0, -1, 0)
var Zp = vec.V3i(0, 0, 1)
var Zm = vec.V3i(0, 0, -1)

var Orientations = []*vec.Mat33i{
	// Zp forward
	vec.Mat33FromVec(Xp, Yp, Zp), // default orientation
	vec.Mat33FromVec(Xp, Yp, Zp).RotZR(1),
	vec.Mat33FromVec(Xp, Yp, Zp).RotZR(2),
	vec.Mat33FromVec(Xp, Yp, Zp).RotZR(3),
	// Zm forward
	vec.Mat33FromVec(Xm, Yp, Zm),
	vec.Mat33FromVec(Xm, Yp, Zm).RotZR(1),
	vec.Mat33FromVec(Xm, Yp, Zm).RotZR(2),
	vec.Mat33FromVec(Xm, Yp, Zm).RotZR(3),
	// Xp forward
	vec.Mat33FromVec(Zm, Yp, Xp),
	vec.Mat33FromVec(Zm, Yp, Xp).RotZR(1),
	vec.Mat33FromVec(Zm, Yp, Xp).RotZR(2),
	vec.Mat33FromVec(Zm, Yp, Xp).RotZR(3),
	// Xm forward
	vec.Mat33FromVec(Zp, Yp, Xm),
	vec.Mat33FromVec(Zp, Yp, Xm).RotZR(1),
	vec.Mat33FromVec(Zp, Yp, Xm).RotZR(2),
	vec.Mat33FromVec(Zp, Yp, Xm).RotZR(3),
	// Yp forward
	vec.Mat33FromVec(Xp, Zm, Yp),
	vec.Mat33FromVec(Xp, Zm, Yp).RotZR(1),
	vec.Mat33FromVec(Xp, Zm, Yp).RotZR(2),
	vec.Mat33FromVec(Xp, Zm, Yp).RotZR(3),
	// Ym forward
	vec.Mat33FromVec(Xp, Zp, Ym),
	vec.Mat33FromVec(Xp, Zp, Ym).RotZR(1),
	vec.Mat33FromVec(Xp, Zp, Ym).RotZR(2),
	vec.Mat33FromVec(Xp, Zp, Ym).RotZR(3),
}

func parse(in string) *context {
	ctx := &context{}
	var sc *scanner
	var scannerRE = regexp.MustCompile(`^--- scanner \d+ ---$`)
	var emptyRE = regexp.MustCompile(`^\s*$`)
	for _, line := range shared.LinesToStringArray(in) {
		switch {
		case scannerRE.MatchString(line):
			sc = &scanner{
				// Hacky AF
				Index:   shared.ParseInt(strings.TrimSpace(line[12:14])),
				Beacons: Beacons{},
			}
			ctx.Scanners = append(ctx.Scanners, sc)
			continue
		case emptyRE.MatchString(line):
			continue
		default:
			sc.Beacons[*vec.Vec3iFromCommaList(line)] = set.Void{}
		}
	}
	return ctx
}

func (s *scanner) FindAlignment(beacons Beacons) *alignment {
	var bestAlignment *alignment
	// Needs at least 12 matches, so best score starts out at 11
	bestScore := 11
	for i := 0; i < len(Orientations); i++ {
		// This map counts what translation would yield how many positive alignments. Preallocate it to reduce the
		// number of map-growths.
		translations := make(map[vec.Vec3i]int, len(beacons)*len(s.Beacons))
		for sb := range s.Beacons {
			// Orient the scanner's position correctly
			orientedPos := Orientations[i].Transform(&sb)
			for beaconPos := range beacons {
				// Compute the translation vector from the rotated scanner's position to the beacon position
				diff := beaconPos.Sub(orientedPos)
				// Count in the translations map
				translations[*diff] = translations[*diff] + 1
			}
		}
		// Compute this orientations best score
		for t, n := range translations {
			if n > bestScore {
				bestAlignment = &alignment{
					translation: t.Copy(),
					orientation: i,
				}
				bestScore = n
			}
		}
	}
	return bestAlignment
}

func (b Beacons) Transform(align *alignment) Beacons {
	out := Beacons{}
	o := Orientations[align.orientation]
	t := align.translation
	for pos := range b {
		v := o.Transform(&pos).Add(t)
		out[*v] = set.Void{}
	}
	return out
}

func (b Beacons) MergeFrom(other Beacons) {
	for v := range other {
		b[v] = set.Void{}
	}
}

func (c *context) BuildMap() Beacons {
	beacons := c.Scanners[0].Beacons
	c.Scanners[0].Align = &alignment{
		translation: vec.V3i(0, 0, 0),
		orientation: 0,
	}
	todo := c.Scanners[1:]
	for len(todo) > 0 {
		startLen := len(todo)
		next := todo[:]
		todo = todo[0:0]
		for _, sc := range next {
			// Compute alignment
			align := sc.FindAlignment(beacons)
			if align == nil {
				// Retry later
				todo = append(todo, sc)
				continue
			}
			// Add beacons if they're not duplicates
			sc.Align = align
			beacons.MergeFrom(sc.Beacons.Transform(align))
		}
		if len(todo) == startLen {
			log.Fatalf("Not converging!")
		}
	}
	return beacons
}

func (c *context) MaxDistance() int {
	maxDistance := 0
	for _, sca := range c.Scanners {
		for _, scb := range c.Scanners {
			d := sca.Align.translation.DistanceL1(scb.Align.translation)
			if d > maxDistance {
				maxDistance = d
			}
		}
	}
	return maxDistance
}

func partA(in string) interface{} {
	ctx := parse(in)
	return len(ctx.BuildMap())
}

func partB(in string) interface{} {
	ctx := parse(in)
	ctx.BuildMap()
	return ctx.MaxDistance()
}

func Test(t *testing.T) {
	day := int64(19)
	testInput := "--- scanner 0 ---\n404,-588,-901\n528,-643,409\n-838,591,734\n390,-675,-793\n-537,-823,-458\n-485,-357,347\n-345,-311,381\n-661,-816,-575\n-876,649,763\n-618,-824,-621\n553,345,-567\n474,580,667\n-447,-329,318\n-584,868,-557\n544,-627,-890\n564,392,-477\n455,729,728\n-892,524,684\n-689,845,-530\n423,-701,434\n7,-33,-71\n630,319,-379\n443,580,662\n-789,900,-551\n459,-707,401\n\n--- scanner 1 ---\n686,422,578\n605,423,415\n515,917,-361\n-336,658,858\n95,138,22\n-476,619,847\n-340,-569,-846\n567,-361,727\n-460,603,-452\n669,-402,600\n729,430,532\n-500,-761,534\n-322,571,750\n-466,-666,-811\n-429,-592,574\n-355,545,-477\n703,-491,-529\n-328,-685,520\n413,935,-424\n-391,539,-444\n586,-435,557\n-364,-763,-893\n807,-499,-711\n755,-354,-619\n553,889,-390\n\n--- scanner 2 ---\n649,640,665\n682,-795,504\n-784,533,-524\n-644,584,-595\n-588,-843,648\n-30,6,44\n-674,560,763\n500,723,-460\n609,671,-379\n-555,-800,653\n-675,-892,-343\n697,-426,-610\n578,704,681\n493,664,-388\n-671,-858,530\n-667,343,800\n571,-461,-707\n-138,-166,112\n-889,563,-600\n646,-828,498\n640,759,510\n-630,509,768\n-681,-892,-333\n673,-379,-804\n-742,-814,-386\n577,-820,562\n\n--- scanner 3 ---\n-589,542,597\n605,-692,669\n-500,565,-823\n-660,373,557\n-458,-679,-417\n-488,449,543\n-626,468,-788\n338,-750,-386\n528,-832,-391\n562,-778,733\n-938,-730,414\n543,643,-506\n-524,371,-870\n407,773,750\n-104,29,83\n378,-903,-323\n-778,-728,485\n426,699,580\n-438,-605,-362\n-469,-447,-387\n509,732,623\n647,635,-688\n-868,-804,481\n614,-800,639\n595,780,-596\n\n--- scanner 4 ---\n727,592,562\n-293,-554,779\n441,611,-461\n-714,465,-776\n-743,427,-804\n-660,-479,-426\n832,-632,460\n927,-485,-438\n408,393,-506\n466,436,-512\n110,16,151\n-258,-428,682\n-393,719,612\n-211,-452,876\n808,-476,-593\n-575,615,604\n-485,667,467\n-680,325,-822\n-627,-443,-432\n872,-547,-609\n833,512,582\n807,604,487\n839,-516,451\n891,-625,532\n-652,-548,-490\n30,-46,-14"
	assert.EqualValues(t, 79, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 3621, partB(testInput))
	shared.RunDay(partB, day, "b")
}
