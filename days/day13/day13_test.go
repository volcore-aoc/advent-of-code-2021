package day13

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/grid"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"gitlab.com/volcore-aoc/advent-of-code-2021/vec"
	"math"
	"strings"
	"testing"
)

type fold struct {
	dir      string
	distance int
}

type context struct {
	dots  []*vec.Vec2[int]
	folds []*fold
}

func (c *context) resolve(v *vec.Vec2[int], maxFolds int) *vec.Vec2[int] {
	x := v.X
	y := v.Y
	for idx, fold := range c.folds {
		if idx >= maxFolds {
			break
		}
		if fold.dir == "x" {
			if x > fold.distance {
				x = 2*fold.distance - x
			}
		} else {
			if y > fold.distance {
				y = 2*fold.distance - y
			}
		}
	}
	return &vec.Vec2[int]{x, y}
}

func parse(in string) *context {
	ctx := &context{}
	for _, line := range shared.LinesToStringArray(in) {
		if strings.Contains(line, ",") {
			ctx.dots = append(ctx.dots, vec.Vec2iFromCommaList(line))
		} else if strings.Contains(line, "fold ") {
			lr := strings.Split(line[11:], "=")
			ctx.folds = append(ctx.folds, &fold{
				dir:      lr[0],
				distance: shared.ParseInt(lr[1]),
			})
		}
	}
	return ctx
}

func partA(in string) interface{} {
	ctx := parse(in)
	// Add dots
	w := 2000
	g := map[int]bool{}
	for _, dot := range ctx.dots {
		dot = ctx.resolve(dot, 1)
		g[dot.X+dot.Y*w] = true
	}
	return len(g)
}

func partB(in string) interface{} {
	ctx := parse(in)
	// Add dots
	w := 20000
	g := map[int]bool{}
	min := vec.Vec2[int]{math.MaxInt, math.MaxInt}
	max := vec.Vec2[int]{0, 0}
	for _, dot := range ctx.dots {
		dot = ctx.resolve(dot, 99999)
		g[dot.X+dot.Y*w] = true
		min.Min(dot)
		max.Max(dot)
	}
	// Plot
	grid := grid.GridFromSize[int](max.X+1, max.Y+1)
	for posIdx, _ := range g {
		x := posIdx % w
		y := posIdx / w
		grid.Set(x, y, 1)
	}
	grid.PlotBinary()
	return nil
}

func Test(t *testing.T) {
	day := int64(13)
	testInput := "6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5"
	assert.EqualValues(t, 17, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, nil, partB(testInput))
	shared.RunDay(partB, day, "b")
}
