package day16

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/bitreader"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"math"
	"testing"
)

type packet struct {
	Version    int
	TypeId     int
	Literal    int
	LengthFlag bool
	SubLength  int
	SubPacket  []*packet
}

func (p *packet) VersionSum() int {
	sum := p.Version
	for _, child := range p.SubPacket {
		sum += child.VersionSum()
	}
	return sum
}

func (p *packet) Compute() int {
	subValues := shared.ArrayMapCopy(p.SubPacket, func(child *packet) int {
		return child.Compute()
	})
	switch p.TypeId {
	case 0:
		return shared.ArraySum(subValues)
	case 1:
		return shared.ArrayReduce(subValues, 1, func(t int, v int) int { return t * v })
	case 2:
		return shared.ArrayReduce(subValues, math.MaxInt, func(t int, v int) int { return shared.Min(t, v) })
	case 3:
		return shared.ArrayReduce(subValues, math.MinInt, func(t int, v int) int { return shared.Max(t, v) })
	case 4:
		return p.Literal
	case 5:
		if subValues[0] > subValues[1] {
			return 1
		}
		return 0
	case 6:
		if subValues[0] < subValues[1] {
			return 1
		}
		return 0
	case 7:
		if subValues[0] == subValues[1] {
			return 1
		}
		return 0
	}
	return -1
}

type context struct {
	Packet *packet
}

func parsePacket(br *bitreader.BitReader) *packet {
	p := &packet{
		Version: br.ReadInt(3),
		TypeId:  br.ReadInt(3),
	}
	if p.TypeId == 4 {
		// Literal value
		p.Literal = br.ReadVarInt()
	} else {
		// Operator
		p.LengthFlag = br.ReadInt(1) > 0
		if p.LengthFlag {
			numSub := br.ReadInt(11)
			for i := 0; i < numSub; i++ {
				p.SubPacket = append(p.SubPacket, parsePacket(br))
			}
		} else {
			subLen := br.ReadInt(15)
			start := br.Offset
			for br.Offset-start < subLen {
				p.SubPacket = append(p.SubPacket, parsePacket(br))
			}
		}
	}
	return p
}

func parse(in string) *context {
	ctx := &context{
		Packet: parsePacket(bitreader.NewBitReader(in)),
	}
	return ctx
}

func partA(in string) interface{} {
	ctx := parse(in)
	return ctx.Packet.VersionSum()
}

func partB(in string) interface{} {
	ctx := parse(in)
	return ctx.Packet.Compute()
}

func Test(t *testing.T) {
	day := int64(16)
	testInput := "8A004A801A8002F478"
	testInput2 := "620080001611562C8802118E34"
	testInput3 := "C0015000016115A2E0802F182340"
	testInput4 := "A0016C880162017C3686B18A3D4780"
	assert.EqualValues(t, 16, partA(testInput))
	assert.EqualValues(t, 12, partA(testInput2))
	assert.EqualValues(t, 23, partA(testInput3))
	assert.EqualValues(t, 31, partA(testInput4))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 3, partB("C200B40A82"))
	assert.EqualValues(t, 54, partB("04005AC33890"))
	assert.EqualValues(t, 7, partB("880086C3E88112"))
	assert.EqualValues(t, 9, partB("CE00C43D881120"))
	assert.EqualValues(t, 1, partB("D8005AC2A8F0"))
	assert.EqualValues(t, 0, partB("F600BC2D8F"))
	assert.EqualValues(t, 0, partB("9C005AC2F8F0"))
	assert.EqualValues(t, 1, partB("9C0141080250320F1802104A08"))
	shared.RunDay(partB, day, "b")
}
