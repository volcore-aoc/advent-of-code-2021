package day20

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/grid"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"strings"
	"testing"
)

type context struct {
	decoder string
	image   *grid.Grid[int]
	step    int
}

func parse(in string) *context {
	parts := strings.SplitN(in, "\n\n", 2)
	return &context{
		decoder: strings.TrimSpace(parts[0]),
		image:   grid.IntGridFromImage(parts[1]),
	}
}

func (c *context) Enhance() {
	w, h := c.image.Size()
	if c.decoder[0] == '#' {
		c.image.SetOutside(c.step % 2)
	}
	// Grow image for new pixels
	newImage := grid.GridFromSize[int](w+2, h+2)
	for y := -1; y < h+1; y++ {
		for x := -1; x < w+1; x++ {
			offset :=
				c.image.Get(x-1, y-1)<<8 +
					c.image.Get(x+0, y-1)<<7 +
					c.image.Get(x+1, y-1)<<6 +
					c.image.Get(x-1, y+0)<<5 +
					c.image.Get(x+0, y+0)<<4 +
					c.image.Get(x+1, y+0)<<3 +
					c.image.Get(x-1, y+1)<<2 +
					c.image.Get(x+0, y+1)<<1 +
					c.image.Get(x+1, y+1)<<0
			v := 0
			if c.decoder[offset] == '#' {
				v = 1
			}
			newImage.Set(x+1, y+1, v)
		}
	}
	c.image = newImage
	c.step++
}

func (c *context) Count() int {
	return c.image.Sum()
}

func partA(in string) interface{} {
	ctx := parse(in)
	ctx.Enhance()
	ctx.Enhance()
	return ctx.Count()
}

func partB(in string) interface{} {
	ctx := parse(in)
	for i := 0; i < 50; i++ {
		ctx.Enhance()
	}
	return ctx.Count()
}

func Test(t *testing.T) {
	day := int64(20)
	testInput := "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#\n\n#..#.\n#....\n##..#\n..#..\n..###"
	assert.EqualValues(t, 35, partA(testInput))
	shared.RunDay(partA, day, "a")
	assert.EqualValues(t, 3351, partB(testInput))
	shared.RunDay(partB, day, "b")
}
