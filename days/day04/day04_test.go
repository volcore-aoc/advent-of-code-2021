package day04

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

type day04Player struct {
	board  [][]int64
	marked [][]bool
	hasWon bool
}

type day04Game struct {
	numbers []int64
	players []*day04Player
}

func (p *day04Player) MarkNumber(n int64) {
	for y := 0; y < len(p.board); y++ {
		for x := 0; x < len(p.board[y]); x++ {
			if p.board[y][x] == n {
				p.marked[y][x] = true
				return
			}
		}
	}
}

func (p *day04Player) HasWon() bool {
	for y := 0; y < len(p.board); y++ {
		markedRow := true
		markedColumn := true
		for x := 0; x < len(p.board[y]); x++ {
			markedRow = markedRow && p.marked[y][x]
			markedColumn = markedColumn && p.marked[x][y]
		}
		if markedRow || markedColumn {
			return true
		}
	}
	return false
}

func (p *day04Player) SumUnmarked() int64 {
	sum := int64(0)
	for y := 0; y < len(p.board); y++ {
		for x := 0; x < len(p.board[y]); x++ {
			if !p.marked[y][x] {
				sum += p.board[y][x]
			}
		}
	}
	return sum
}

func (g *day04Game) MarkNumber(number int64) {
	// Mark the next number
	for _, player := range g.players {
		player.MarkNumber(number)
	}
}

func (g *day04Game) PlayA() int64 {
	for _, number := range g.numbers {
		g.MarkNumber(number)
		// Check for victory condition
		for _, player := range g.players {
			if player.HasWon() {
				return player.SumUnmarked() * number
			}
		}
	}
	return -1
}

func (g *day04Game) PlayB() int64 {
	winCount := 0
	for _, number := range g.numbers {
		g.MarkNumber(number)
		// Prune
		for _, player := range g.players {
			if player.HasWon() && !player.hasWon {
				player.hasWon = true
				winCount++
				if winCount == len(g.players) {
					// if this was the last player, we're done
					return player.SumUnmarked() * number
				}
			}
		}
	}
	return -1
}

func day04Parse(in string) *day04Game {
	game := &day04Game{}
	numbers, rem := shared.TakeLines(in, 1)
	game.numbers = shared.CommaListToInt64Array(numbers[0])
	for len(in) > 0 {
		var board []string
		board, rem = shared.TakeLines(rem, 5)
		if len(board) < 5 {
			break
		}
		player := &day04Player{}
		for _, row := range board {
			player.board = append(player.board, shared.WhitespaceListToIntArray(row))
			player.marked = append(player.marked, make([]bool, 5))
		}
		game.players = append(game.players, player)
	}
	return game
}

func day04a(in string) interface{} {
	return day04Parse(in).PlayA()
}

func day04b(in string) interface{} {
	return day04Parse(in).PlayB()
}

func TestDay04(t *testing.T) {
	testInput := "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1\n\n22 13 17 11  0\n 8  2 23  4 24\n21  9 14 16  7\n 6 10  3 18  5\n 1 12 20 15 19\n\n 3 15  0  2 22\n 9 18 13 17  5\n19  8  7 25 23\n20 11 10 24  4\n14 21 16 12  6\n\n14 21 17 24  4\n10 16 15  9 19\n18  8 23 26 20\n22 11 13  6  5\n 2  0 12  3  7"
	assert.EqualValues(t, 4512, day04a(testInput))
	shared.RunDay(day04a, 4, "a")
	assert.EqualValues(t, 1924, day04b(testInput))
	shared.RunDay(day04b, 4, "b")
}
