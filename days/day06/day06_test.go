package day06

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"testing"
)

// Slow and naive reference approach, for unit testing
func day06CountAfterNaive(l []int, days int) int {
	for i := 0; i < days; i++ {
		var nl []int
		for j := 0; j < len(l); j++ {
			if l[j] == 0 {
				nl = append(nl, 8)
				l[j] = 6
			} else {
				l[j] -= 1
			}
		}
		l = append(l, nl...)
	}
	return len(l)
}

// Fast approach, using memory and recursion
var day06Memory = map[int]int64{}

func day06CountSingleAfter(l int, days int) int64 {
	if days <= l {
		return 1
	}
	// Skip days, as f(n, d) == f(0, d-n) if d > n
	if l > 0 {
		return day06CountSingleAfter(0, days-l)
	}
	// Check memory
	if value, ok := day06Memory[days]; ok {
		return value
	}
	// Reproduce
	value := day06CountSingleAfter(8, days-1) + day06CountSingleAfter(6, days-1)
	day06Memory[days] = value
	return value
}

func day06CountAfter(l []int, days int) int64 {
	sum := int64(0)
	for _, v := range l {
		sum += day06CountSingleAfter(v, days)
	}
	return sum
}

// Even faster approach, using buckets and just counting how many are in which bucket
func day06CountFast(l []int, days int) int64 {
	// Create buckets
	b := make([]int64, 9)
	for _, v := range l {
		b[v] += 1
	}
	for i := 0; i < days; i++ {
		born := b[0]
		for j := 0; j < 8; j++ {
			b[j] = b[j+1]
		}
		b[6] += born
		b[8] = born
	}
	return shared.ArraySum(b)
}

func day06a(in string) interface{} {
	return day06CountFast(shared.CommaListToIntArray(in), 80)
}

func day06b(in string) interface{} {
	return day06CountFast(shared.CommaListToIntArray(in), 256)
}

func TestDay06(t *testing.T) {
	for i := 0; i < 30; i++ {
		naive := day06CountAfterNaive([]int{1, 2, 3}, i)
		better := day06CountAfter([]int{1, 2, 3}, i)
		fast := day06CountFast([]int{1, 2, 3}, i)
		assert.EqualValues(t, naive, better)
		assert.EqualValues(t, naive, fast)
	}
	day := int64(6)
	aFunc := day06a
	bFunc := day06b
	testInput := "3,4,3,1,2"
	assert.EqualValues(t, 5934, aFunc(testInput))
	assert.EqualValues(t, 26984457539, bFunc(testInput))
	shared.RunDay(aFunc, day, "a")
	shared.RunDay(bFunc, day, "b")
}
