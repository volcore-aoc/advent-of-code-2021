package day25

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"strings"
	"testing"
)

type context struct {
	rows  []string
	steps int
}

func parse(in string) *context {
	ctx := &context{
		rows: shared.LinesToStringArray(in),
	}
	return ctx
}

func (c *context) step() int {
	moveCount := 0
	// horizontal
	var next []string
	for _, row := range c.rows {
		w := len(row)
		var sb strings.Builder
		sb.Grow(w)
		for i := 0; i < len(row); i++ {
			vp := row[(i+1)%w]
			v := row[i]
			vm := row[(i+w-1)%w]
			switch v {
			case '.':
				if vm == '>' {
					v = '>'
					moveCount++
				}
			case '>':
				if vp == '.' {
					v = '.'
				}
			}
			sb.WriteByte(v)
		}
		next = append(next, sb.String())
	}
	c.rows = next
	// Vertical
	rlen := len(c.rows)
	next = []string{}
	for ri, row := range c.rows {
		rowm := c.rows[(ri+rlen-1)%rlen]
		rowp := c.rows[(ri+1)%rlen]
		w := len(row)
		var sb strings.Builder
		sb.Grow(w)
		for i := 0; i < len(row); i++ {
			vp := rowp[i]
			v := row[i]
			vm := rowm[i]
			switch v {
			case '.':
				if vm == 'v' {
					v = 'v'
					moveCount++
				}
			case 'v':
				if vp == '.' {
					v = '.'
				}
			}
			sb.WriteByte(v)
		}
		next = append(next, sb.String())
	}
	c.rows = next
	c.steps++
	return moveCount
}

func (c *context) dump() {
	log.WithField("Steps", c.steps).Info("After")
	for _, row := range c.rows {
		log.WithField("row", row).Info("Dump")
	}
}
func partA(in string) interface{} {
	ctx := parse(in)
	for {
		count := ctx.step()
		if count == 0 {
			return ctx.steps
		}
	}
}

func Test(t *testing.T) {
	day := int64(25)
	testInput := "v...>>.vv>\n.vv>>.vv..\n>>.>v>...v\n>>v>>.>.v.\nv>v.vv.v..\n>.>>..v...\n.vv..>.>v.\nv.v..>>v.v\n....v..v.>"
	assert.EqualValues(t, 58, partA(testInput))
	shared.RunDay(partA, day, "a")
}
