package day12

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/volcore-aoc/advent-of-code-2021/set"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
	"strings"
	"testing"
)

type graph struct {
	conn map[string][]string
}

func parse(in string) *graph {
	conn := map[string][]string{}
	for _, line := range shared.LinesToStringArray(in) {
		parts := strings.SplitN(line, "-", 2)
		conn[parts[0]] = append(conn[parts[0]], parts[1])
		conn[parts[1]] = append(conn[parts[1]], parts[0])
	}
	return &graph{
		conn: conn,
	}
}

func (g *graph) countPathsRec(node string, visited set.StringSet, secondary string) int {
	if node == "end" {
		return 1
	}
	newVisited := visited
	isLarge := shared.StringIsUpper(node)
	// Is this a large cave?
	if !isLarge {
		// Can we visit this large cave again?
		if visited.IsSet(node) {
			// We can visit one node twice. check if it's this one
			if node == "start" || secondary != "" {
				return 0
			}
			secondary = node
		}
		// Only make a copy if it's modified
		newVisited = visited.Clone()
		newVisited.Set(node)
	}
	// Visit children
	sum := 0
	for _, child := range g.conn[node] {
		sum += g.countPathsRec(child, newVisited, secondary)
	}
	return sum
}

func partA(in string) interface{} {
	g := parse(in)
	return g.countPathsRec("start", set.StringSet{}, "invalid")
}

func partB(in string) interface{} {
	g := parse(in)
	return g.countPathsRec("start", set.StringSet{}, "")
}

func Test(t *testing.T) {
	day := int64(12)
	testInput := "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end"
	testInput2 := "dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc"
	testInput3 := "fs-end\nhe-DX\nfs-he\nstart-DX\npj-DX\nend-zg\nzg-sl\nzg-pj\npj-he\nRW-he\nfs-DX\npj-RW\nzg-RW\nstart-pj\nhe-WI\nzg-he\npj-fs\nstart-RW"
	assert.EqualValues(t, 10, partA(testInput))
	assert.EqualValues(t, 19, partA(testInput2))
	assert.EqualValues(t, 226, partA(testInput3))
	assert.EqualValues(t, 36, partB(testInput))
	assert.EqualValues(t, 103, partB(testInput2))
	assert.EqualValues(t, 3509, partB(testInput3))
	shared.RunDay(partA, day, "a")
	shared.RunDay(partB, day, "b")
}
