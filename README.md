# Advent of Code 2021

Doing [Advent of Code](https://adventofcode.com/2021) again this year. Chose [Golang](https://go.dev).

Run `go test -v ./...` to run all days. I implemented everything in tests as that's the most convenient way for me when
using [GoLand](https://www.jetbrains.com/go/).

Please note, you need to replace the inputs with your own data files in order to get the right results for your account,
as the challenges are individualized.