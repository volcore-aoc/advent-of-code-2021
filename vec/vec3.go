package vec

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
)

type Vec3[T shared.Number] struct {
	X T
	Y T
	Z T
}

func V3[T shared.Number](x T, y T, z T) *Vec3[T] {
	return &Vec3[T]{x, y, z}
}

func (v *Vec3[T]) Copy() *Vec3[T] {
	return &Vec3[T]{
		X: v.X,
		Y: v.Y,
		Z: v.Z,
	}
}

func (v *Vec3[T]) Add(o *Vec3[T]) *Vec3[T] {
	return &Vec3[T]{
		X: v.X + o.X,
		Y: v.Y + o.Y,
		Z: v.Z + o.Z,
	}
}

func (v *Vec3[T]) Sub(o *Vec3[T]) *Vec3[T] {
	return &Vec3[T]{
		X: v.X - o.X,
		Y: v.Y - o.Y,
		Z: v.Z - o.Z,
	}
}

func (v *Vec3[T]) Scale(s T) *Vec3[T] {
	return &Vec3[T]{
		X: v.X * s,
		Y: v.Y * s,
		Z: v.Z * s,
	}
}

func (v *Vec3[T]) MulAdd(s T, o *Vec3[T]) *Vec3[T] {
	return &Vec3[T]{
		X: v.X + s*o.X,
		Y: v.Y + s*o.Y,
		Z: v.Z + s*o.Z,
	}
}

func (v *Vec3[T]) Min(o *Vec3[T]) *Vec3[T] {
	return &Vec3[T]{
		X: shared.Min(v.X, o.X),
		Y: shared.Min(v.Y, o.Y),
		Z: shared.Min(v.Z, o.Z),
	}
}

func (v *Vec3[T]) Max(o *Vec3[T]) *Vec3[T] {
	return &Vec3[T]{
		X: shared.Max(v.X, o.X),
		Y: shared.Max(v.Y, o.Y),
		Z: shared.Max(v.Z, o.Z),
	}
}

func (v *Vec3[T]) L1() T {
	return shared.Abs(v.X) + shared.Abs(v.Y) + shared.Abs(v.Z)
}

func (v *Vec3[T]) DistanceL1(b *Vec3[T]) T {
	return b.Sub(v).L1()
}

func (v *Vec3[T]) String() string {
	return fmt.Sprintf("[%v %v %v]", v.X, v.Y, v.Z)
}

type Vec3i = Vec3[int]

func Vec3iFromCommaList(s string) *Vec3i {
	l := shared.CommaListToIntArray(s)
	if len(l) < 3 {
		log.WithField("s", s).WithField("l", l).Fatalf("Trying to parse Vec3[int] but not enough fields")
	}
	return &Vec3i{l[0], l[1], l[2]}
}

func V3i(x int, y int, z int) *Vec3i {
	return &Vec3i{x, y, z}
}
