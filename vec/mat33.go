package vec

import (
	"constraints"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
)

// 0 1 2
// 3 4 5
// 6 7 8
type Mat33[T constraints.Signed] struct {
	M []T
}

func (m *Mat33[T]) Transform(v *Vec3[T]) *Vec3[T] {
	return V3[T](
		m.M[0]*v.X+m.M[1]*v.Y+m.M[2]*v.Z,
		m.M[3]*v.X+m.M[4]*v.Y+m.M[5]*v.Z,
		m.M[6]*v.X+m.M[7]*v.Y+m.M[8]*v.Z,
	)
}

func (m *Mat33[T]) X() *Vec3[T] {
	return V3[T](m.M[0], m.M[3], m.M[6])
}

func (m *Mat33[T]) Y() *Vec3[T] {
	return V3[T](m.M[1], m.M[4], m.M[7])
}

func (m *Mat33[T]) Z() *Vec3[T] {
	return V3[T](m.M[2], m.M[5], m.M[8])
}

func (m *Mat33[T]) RotXR(count int) *Mat33[T] {
	switch shared.PositiveMod(count, 4) {
	default:
		return m
	case 1:
		return Mat33FromVec[T](m.X(), m.Z().Scale(-1), m.Y())
	case 2:
		return Mat33FromVec[T](m.X(), m.Y().Scale(-1), m.Z().Scale(-1))
	case 3:
		return Mat33FromVec[T](m.X(), m.Z(), m.Y().Scale(-1))
	}
}

func (m *Mat33[T]) RotYR(count int) *Mat33[T] {
	switch shared.PositiveMod(count, 4) {
	default:
		return m
	case 1:
		return Mat33FromVec[T](m.Z().Scale(-1), m.Y(), m.X())
	case 2:
		return Mat33FromVec[T](m.X().Scale(-1), m.Y(), m.Z().Scale(-1))
	case 3:
		return Mat33FromVec[T](m.Z(), m.Y(), m.X().Scale(-1))
	}
}

func (m *Mat33[T]) RotZR(count int) *Mat33[T] {
	switch shared.PositiveMod(count, 4) {
	default:
		return m
	case 1:
		return Mat33FromVec[T](m.Y().Scale(-1), m.X(), m.Z())
	case 2:
		return Mat33FromVec[T](m.X().Scale(-1), m.Y().Scale(-1), m.Z())
	case 3:
		return Mat33FromVec[T](m.Y(), m.X().Scale(-1), m.Z())
	}
}

func Mat33FromVec[T constraints.Signed](x *Vec3[T], y *Vec3[T], z *Vec3[T]) *Mat33[T] {
	return &Mat33[T]{
		M: []T{
			x.X, y.X, z.X,
			x.Y, y.Y, z.Y,
			x.Z, y.Z, z.Z,
		},
	}
}

type Mat33i = Mat33[int]
