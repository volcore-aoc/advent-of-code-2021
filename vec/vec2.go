package vec

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
)

type Vec2[T shared.Number] struct {
	X T
	Y T
}

func V2[T shared.Number](x T, y T) *Vec2[T] {
	return &Vec2[T]{x, y}
}

func (v *Vec2[T]) Add(o *Vec2[T]) *Vec2[T] {
	return &Vec2[T]{
		X: v.X + o.X,
		Y: v.Y + o.Y,
	}
}

func (v *Vec2[T]) AddXY(x T, y T) *Vec2[T] {
	return &Vec2[T]{
		X: v.X + x,
		Y: v.Y + y,
	}
}

func (v *Vec2[T]) MulAdd(s T, o *Vec2[T]) *Vec2[T] {
	return &Vec2[T]{
		X: v.X + s*o.X,
		Y: v.Y + s*o.Y,
	}
}

func (v *Vec2[T]) Sub(o *Vec2[T]) *Vec2[T] {
	return &Vec2[T]{
		X: v.X - o.X,
		Y: v.Y - o.Y,
	}
}

func (v *Vec2[T]) L1() T {
	return shared.Abs(v.X) + shared.Abs(v.Y)
}

func (v *Vec2[T]) Divide(s T) *Vec2[T] {
	return &Vec2[T]{
		X: v.X / s,
		Y: v.Y / s,
	}
}

func (v *Vec2[T]) IsEqual(o *Vec2[T]) bool {
	return v.X == o.X && v.Y == o.Y
}

func (v *Vec2[T]) Max(o *Vec2[T]) {
	if o.X > v.X {
		v.X = o.X
	}
	if o.Y > v.Y {
		v.Y = o.Y
	}
}

func (v *Vec2[T]) Min(o *Vec2[T]) {
	if o.X < v.X {
		v.X = o.X
	}
	if o.Y < v.Y {
		v.Y = o.Y
	}
}

// Specialization
type Vec2i64 = Vec2[int64]
type Vec2i = Vec2[int]

func Vec2i64FromCommaList(s string) *Vec2i64 {
	l := shared.CommaListToInt64Array(s)
	if len(l) < 2 {
		log.WithField("s", s).WithField("l", l).Fatalf("Trying to parse Vec2[int64] but not enough fields")
	}
	return &Vec2i64{l[0], l[1]}
}

func Vec2iFromCommaList(s string) *Vec2i {
	l := shared.CommaListToIntArray(s)
	if len(l) < 2 {
		log.WithField("s", s).WithField("l", l).Fatalf("Trying to parse Vec2[int] but not enough fields")
	}
	return &Vec2i{l[0], l[1]}
}

func Vec2iFromStringArray(s []string) *Vec2i {
	return &Vec2i{shared.ParseInt(s[0]), shared.ParseInt(s[1])}
}
