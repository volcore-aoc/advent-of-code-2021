package grid

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/volcore-aoc/advent-of-code-2021/shared"
)

type Grid[T shared.Number] struct {
	grid         [][]T
	outsideValue T
}

func IntGridFromString(in string) *Grid[int] {
	return &Grid[int]{
		grid: shared.GridToIntArrays(in),
	}
}

func IntGridFromImage(in string) *Grid[int] {
	return &Grid[int]{
		grid: shared.ImageToIntArrays(in),
	}
}

func GridFromSize[T shared.Number](w int, h int) *Grid[T] {
	g := make([][]T, h)
	for i := 0; i < h; i++ {
		g[i] = make([]T, w)
	}
	return &Grid[T]{
		grid: g,
	}
}

func (g *Grid[T]) NumCells() int {
	w, h := g.Size()
	return w * h
}

func (g *Grid[T]) Size() (int, int) {
	return len(g.grid[0]), len(g.grid)
}

func (g *Grid[T]) SetOutside(v T) {
	g.outsideValue = v
}

func (g *Grid[T]) Inside(x int, y int) bool {
	return x >= 0 && x < len(g.grid[0]) && y >= 0 && y < len(g.grid)
}

func (g *Grid[T]) Set(x int, y int, v T) {
	if !g.Inside(x, y) {
		return
	}
	g.grid[y][x] = v
}

func (g *Grid[T]) Get(x int, y int) T {
	if !g.Inside(x, y) {
		return g.outsideValue
	}
	return g.grid[y][x]
}

func (g *Grid[T]) Apply(f func(x int, y int, v T)) {
	w, h := g.Size()
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			f(x, y, g.Get(x, y))
		}
	}
}

func (g *Grid[T]) Map(f func(x int, y int, v T) T) {
	w, h := g.Size()
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			g.Set(x, y, f(x, y, g.Get(x, y)))
		}
	}
}

func (g *Grid[T]) Reduce(start T, f func(x int, y int, v T, total T) T) T {
	w, h := g.Size()
	total := start
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			total = f(x, y, g.Get(x, y), total)
		}
	}
	return total
}

func (g *Grid[T]) Inc(x int, y int, v T) {
	g.Set(x, y, g.Get(x, y)+v)
}

func (g *Grid[T]) Sum() T {
	return g.Reduce(0, func(x int, y int, v T, total T) T {
		return v + total
	})
}

func (g *Grid[T]) SplatInc(x int, y int, rx int, ry int, inc T) {
	for ny := y - ry; ny <= y+ry; ny++ {
		for nx := x - rx; nx <= x+rx; nx++ {
			g.Set(nx, ny, g.Get(nx, ny)+inc)
		}
	}
}

func (g *Grid[T]) Linearize() []T {
	var l []T
	g.Apply(func(x int, y int, v T) {
		l = append(l, v)
	})
	return l
}

func (g *Grid[T]) PlotBinary() {
	for idx, row := range g.grid {
		min, max := shared.ArrayMinMax(row)
		if min == 0 && max == 0 {
			continue
		}
		s := ""
		for _, c := range row {
			if c > 0 {
				s += "#"
			} else {
				s += "."
			}
		}
		log.WithField("idx", idx).WithField("s", s).Info("Plot")
	}
}

func (g *Grid[T]) PlotDec() {
	for idx, row := range g.grid {
		s := ""
		for _, c := range row {
			s += string(rune((int64(c) % 10) + '0'))
		}
		log.WithField("idx", idx).WithField("s", s).Info("Plot")
	}
}

func (g *Grid[T]) Repeat(cx int, cy int) {
	var grid [][]T
	for j := 0; j < cy; j++ {
		for _, row := range g.grid {
			var newRow []T
			for i := 0; i < cy; i++ {
				newRow = append(newRow, row...)
			}
			grid = append(grid, newRow)
		}
	}
	g.grid = grid
}
